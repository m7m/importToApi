DROP TABLE IF EXISTS `rel_categories`;

CREATE TABLE rel_categories (
id INT auto_increment NOT NULL primary key,
status TINYINT DEFAULT 0 COMMENT '-1 = non permit, 0 = désactivé, 1 = actif sur id_category, 2 = id_child, 3 = id_parent',

id_parent INT NOT NULL COMMENT 'Id parent lié à l id parent de prestashop(ps_category.id_parent)',
id_category INT NOT NULL COMMENT 'Id catégorie lié à l id catégorie de prestashop(ps_category.id_category)',

icecat CHAR(255) charset utf8 DEFAULT NULL, -- obsolete
supplier CHAR(32) charset utf8 NOT NULL,
category TEXT(1024) charset utf8 NOT NULL,

date_add TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'AUTO',
date_upd TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'AUTO',

key id_category(id_category),
key also(also(128)),
key ecp(ecp(128)),
key acadia(acadia(128))

) COMMENT 'Relation entre categories fournisseur.';

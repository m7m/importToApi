#!/bin/sh

WORK_PATH="$(dirname $0)"
cd $WORK_PATH

if [ ! -e 'config/launcher.icomparateur.conf' ];then
	echo 'Fichier de configuration inexistant: config/launcher.icomparateur.conf'
	exit 1
fi
. config/launcher.icomparateur.conf

exec 1>$FILE_LOG
exec 2>&1

send_mel()
{
	return 
    SUBJECT="$1"
    if [ -s $FILE_LOG ];then
	OPT="-a $FILE_LOG"
    else
	OPT=""
    fi
    (
	if [ -s $FILE_LOG ];then
	    echo 'Log ci-joint'
	fi
    ) | /usr/bin/mailx -s "$SUBJECT" $OPT $ADDRE
}

#mysql a besoin de SET GLOBAL log_bin_trust_function_creators = 1;

func_opt_sql_add_func()
{
    mysql -u $USER -p$MDP -D $BDD_SQL \
	  -e "
DROP FUNCTION IF EXISTS foncConcatCategoryName;
DELIMITER //
CREATE DEFINER = CURRENT_USER() FUNCTION foncConcatCategoryName ( id_cat INT, id_lang INT)
RETURNS TEXT(2048) DETERMINISTIC
BEGIN
   SET @id = id_cat;
   SET @cat_name_concat = NULL;
   label1: WHILE @id > 2 DO
	SELECT c.id_category,
	 cl.name,
	 c.id_parent
	 INTO @id_cat, @cat_name, @id_parent
 	 FROM ps_category AS c,
	 ps_category_lang AS cl
 	 WHERE c.id_category = @id
	 AND cl.id_category = c.id_category
	 AND cl.id_lang = id_lang;
	 SET @id = @id_parent;
	 IF @cat_name_concat IS NULL THEN
	 	 SET @cat_name_concat = @cat_name;
	 ELSE
	 	 SET @cat_name_concat = CONCAT(@cat_name, '>', @cat_name_concat);
	 END IF;
   END WHILE label1;
   RETURN @cat_name_concat;
END; //
DELIMITER ;
"
    if [ $? -ne 0 ];then
	echo 'Error add function'
	send_mel 'Extract icompareur: Error add function'
	exit 1
    fi
} # func_opt_sql_add_func

fonc_opt_icomparateur_sql()
{
    reste=$1
    sortie=$2
    limit=$3

    rm $EMP_SQL_ICOM 2>/dev/null
    mysql -u $USER -p$MDP -D $BDD_SQL \
	  -e "

set @TVA = $TVA;
set @id_lang = $ID_LANG;
set @reste = $reste;
set @url=(select CONCAT(domain_ssl,physical_uri) from ps_shop_url where id_shop_url = 1);
set @proto = 'https://';

select 
pl.name,
pm.name as marque,
ps.product_supplier_reference as ref_fournisseur,
p.reference as ref_fabricant,
p.ean13,

CASE
 WHEN foncConcatCategoryName(cl.id_category, @id_lang)
  -- REGEXP '^Systemes>Tablette Tactile($|>Tablette$)'
  REGEXP '^>Ordinateur fixe($|>Ord.*($|>(PC.*|AIO.*))|>S.*($|>(Station.*|DAS.*|SERver.*)))'
      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)
 WHEN foncConcatCategoryName(cl.id_category, @id_lang)
  -- REGEXP '^Systemes>Noteboo(k>[^JAR]+.*|k$)'
  REGEXP '^Ordinateur portable($|>Ordinateur portable($|>[PU].*))'
      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)
 WHEN foncConcatCategoryName(cl.id_category, @id_lang)
  -- REGEXP '^Integration>Cartes Mères(.*|$)'
  REGEXP '^>Composant([^S].*)'
      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)
 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang)
 --  REGEXP '^Systemes>Mini Barebone(s>[^Ap]+.*|s$)'
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang) 
 --  REGEXP '^Peripheriques>Boîtiers externe(s 2.5>[^E]+.*|s 2.5$|s 3.5(.*|$))'
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 WHEN foncConcatCategoryName(cl.id_category, @id_lang) 
  -- REGEXP '^Integration>Disque (D|d)ur( >.*|$| externe(>.*|$)| hybride(>.*|$)| interne(>[^A]+.*|$)| SSD(>.*|$))'
  REGEXP '^Stockage>(Disque|C).*'
      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang) 
 --  REGEXP '^Integration>Système RAID'
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 WHEN foncConcatCategoryName(cl.id_category, @id_lang)
  -- REGEXP '^Peripheriques>Ecran LCD-LED - Moniteur(>[^AS]+.*|$)'
  REGEXP '^Affichage>(Aff[^>]*>Affich.*|Moni[^>]*>Moni.*|Vid.*|Ecran.*)'
      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)
 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang)
 --  REGEXP '^Peripheriques>Ecran LCD - TV(>[^AS]+.*|$)'
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)
 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang)
 --  REGEXP '^Peripheriques>Ecran LCD( - Sur| - Tac).*'
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang)
 --  REGEXP '^Systemes>Systeme(>[^J].*|$)'
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang)
 --  REGEXP 'Multimedia.*>Boitier multimédia'
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang)
 --  REGEXP '^Integration>Boîtiers Gaming'
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang)
 --  REGEXP '^Integration>Cartes Vidéos>Cartes vid.*'
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang)
 --  REGEXP '^Integration>Processeur.*'
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 WHEN foncConcatCategoryName(cl.id_category, @id_lang)
  -- REGEXP '^Peripheriques>Imprimantes.*'
  REGEXP '^Impression>(Imprimantes|Multifonction)[^>]*>(Imprimantes|Multifonction).*'
      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang)
 --  REGEXP '^Peripheriques>Scanners.*'
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang)
 --  REGEXP '^Peripheriques>Fax.*'
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 WHEN foncConcatCategoryName(cl.id_category, @id_lang)
  -- REGEXP '^Reseaux>Courant porteur -CPL-.*'
  REGEXP '^Reseau>Onduleur[^>]*>Courant porteur.*'
      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 WHEN foncConcatCategoryName(cl.id_category, @id_lang)
  -- REGEXP '^Reseaux>Onduleurs(>[^A]+.*|$)'
  REGEXP '^Reseau>Reseau et Telecom[^>]*>Point acces et pont.*'
      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 WHEN foncConcatCategoryName(cl.id_category, @id_lang)
  -- REGEXP '^Reseaux>Routeur-Modem.*'
  REGEXP '^Reseau>Reseau et Telecom[^>]*>Routeur.*'
      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 WHEN foncConcatCategoryName(cl.id_category, @id_lang)
  -- REGEXP '^Reseaux>Sans fil Wifi.*'
  REGEXP '^Reseau>Reseau et Telecom[^>]*>Switch.*'
      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 WHEN foncConcatCategoryName(cl.id_category, @id_lang)
  -- REGEXP '^Reseaux>Switchs et Convertisseurs.*'
  REGEXP '^Reseau>Reseau et Telecom[^>]*>Modem.*'
      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)
 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang)
 --  REGEXP '^Reseaux>Switchs Manageables.*'
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang)
 --  REGEXP '^Peripheriques>Souris .*'
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 -- WHEN foncConcatCategoryName(cl.id_category, @id_lang)
 --  REGEXP '^Integration>Cartes d\'extension.*' -- '
 --      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 WHEN foncConcatCategoryName(cl.id_category, @id_lang)
  REGEXP '^Integration>Clé USB.*'
      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)
 WHEN foncConcatCategoryName(cl.id_category, @id_lang)
  REGEXP '^Integration>Mémoire Flash.*'
      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)
 WHEN foncConcatCategoryName(cl.id_category, @id_lang)
  REGEXP '^Integration>Mémoire Vive.*'
      THEN if(@reste = 0,foncConcatCategoryName(cl.id_category, @id_lang),null)

 WHEN @reste = 1
      THEN foncConcatCategoryName(cl.id_category, @id_lang)
END as categories,

truncate(p.price * @TVA,2) as prix_ttc,
CASE
WHEN p.weight >=  0 && p.weight < 0.250001 THEN truncate(4.900000*@TVA,2)
 WHEN p.weight >= 0.250001 && p.weight < 0.500001 THEN truncate(6.100000*@TVA,2)
  WHEN p.weight >= 0.500001 && p.weight < 0.750001 THEN truncate(6.900000*@TVA,2)
   WHEN p.weight >= 0.750001 && p.weight < 1.000001 THEN truncate(7.500000*@TVA,2)
    WHEN p.weight >= 1.000001 && p.weight < 2.000001 THEN truncate(8.500000*@TVA,2)
     WHEN p.weight >= 2.000001 && p.weight < 5.000001 THEN truncate(12.900000*@TVA,2)
      WHEN p.weight >= 5.000001 && p.weight < 10.000001 THEN truncate(18.900000*@TVA,2)
       WHEN p.weight >= 10.000001 && p.weight < 30.000001 THEN truncate(26.900000*@TVA,2)
           ELSE truncate(26.900000*@TVA,2)
END AS frais_port,

p.ecotax,
CASE
  WHEN sa.quantity > 3
      THEN 'En stock'
  WHEN sa.quantity <= 3
      THEN 'Stock limité'
END as disponibilite,

'' as Description,
'' as Texte_Promo,
concat(@proto,@url,'lang/',p.id_product,'-',pl.link_rewrite,'.html') as url_produit,
concat(@proto,@url, pi.id_image, '-large_default/', pl.link_rewrite, '.jpg') as image_taille_normale,
p.weight as poids

INTO OUTFILE '$EMP_SQL_ICOM'
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '\"'
LINES TERMINATED BY '\n'

from
ps_product as p,
ps_product_lang as pl,
ps_manufacturer as pm,
ps_product_supplier as ps,
ps_category_product AS cp,
ps_category_lang AS cl,
ps_image as pi,
ps_stock_available as sa

where 
    pm.id_manufacturer = p.id_manufacturer
and ps.id_product = p.id_product
and ps.id_product is not null
and ps.id_product != ''
and p.id_product = cp.id_product
and cp.id_category = cl.id_category
and p.id_product = pl.id_product
and p.id_product = pi.id_product
and p.id_product = sa.id_product
and cl.id_lang = @id_lang
and p.active = 1
and sa.quantity > 0

group by p.id_product
HAVING categories is not null
limit 0, $limit;
"

    if [ $? -ne 0 ];then
	echo 'Error extract bdd'
	send_mel 'Extract icompareur: Error extract bdd'
	exit 1
    fi
    cat "$EMP_SQL_ICOM" >> "$sortie"
    if [ $? -ne 0 ];then
	echo 'Error mv file'
	send_mel 'Extract icompareur: Error mv file'
	exit 1
    fi
    rm "$EMP_SQL_ICOM"
    if [ $? -ne 0 ];then
	echo 'Error delete file'
	send_mel 'Extract icompareur: Error delete file'
	exit 1
    fi
} # end fonc_opt_icomparateur_sql

fonc_opt_comparateur_tout_pro_sql()
{
    reste=$1
    sortie=$2
    limit=$3

    rm $EMP_SQL_COM_GLOBAL 2>/dev/null
    mysql $OPT_SQL -D $BDD_SQL \
	-e "
use $BDD_SQL;

set @TVA = $TVA;
set @id_lang = $ID_LANG;
set @reste = $reste; -- n'as pas atteint les 2000 on repase en ignorant les categories précédentes
set @url=(select CONCAT(domain_ssl,physical_uri) from ps_shop_url where id_shop_url = 1);
set @proto = 'https://';

select 
pl.name,
pm.name as marque,
ps.product_supplier_reference as ref_fournisseur,
p.reference as ref_fabricant,
p.ean13,
foncConcatCategoryName(cl.id_category, @id_lang) as categories,

truncate(p.price * @TVA,2) as prix_ttc,
CASE
WHEN p.weight >=  0 && p.weight < 0.250001 THEN truncate(4.900000*@TVA,2)
 WHEN p.weight >= 0.250001 && p.weight < 0.500001 THEN truncate(6.100000*@TVA,2)
  WHEN p.weight >= 0.500001 && p.weight < 0.750001 THEN truncate(6.900000*@TVA,2)
   WHEN p.weight >= 0.750001 && p.weight < 1.000001 THEN truncate(7.500000*@TVA,2)
    WHEN p.weight >= 1.000001 && p.weight < 2.000001 THEN truncate(8.500000*@TVA,2)
     WHEN p.weight >= 2.000001 && p.weight < 5.000001 THEN truncate(12.900000*@TVA,2)
      WHEN p.weight >= 5.000001 && p.weight < 10.000001 THEN truncate(18.900000*@TVA,2)
       WHEN p.weight >= 10.000001 && p.weight < 30.000001 THEN truncate(26.900000*@TVA,2)
           ELSE truncate(26.900000*@TVA,2)
END AS frais_port,

p.ecotax,
CASE
  WHEN sa.quantity > 3
      THEN 'En stock'
  WHEN sa.quantity <= 3
      THEN 'Stock limité'
END as disponibilite,

'' as Description,
'' as Texte_Promo,
concat(@proto,@url,'lang/',p.id_product,'-',pl.link_rewrite,'.html') as url_produit,
concat(@proto,@url, pi.id_image, '-large_default/', pl.link_rewrite, '.jpg') as image_taille_normale,
p.weight as poids

INTO OUTFILE '$EMP_SQL_COM_GLOBAL'
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '\"'
LINES TERMINATED BY '\n'

from
ps_product as p,
ps_product_lang as pl,
ps_manufacturer as pm,
ps_product_supplier as ps,
ps_category_product AS cp,
ps_category_lang AS cl,
ps_image as pi,
ps_stock_available as sa

where 
    pm.id_manufacturer = p.id_manufacturer
and ps.id_product = p.id_product
and ps.id_product is not null
and ps.id_product != ''
and p.id_product = cp.id_product
and cp.id_category = cl.id_category
and p.id_product = pl.id_product
and p.id_product = pi.id_product
and p.id_product = sa.id_product
and cl.id_lang = @id_lang
and p.active = 1
and sa.quantity > 0

group by p.id_product
HAVING categories is not null
"
    if [ $? -ne 0 ];then
	echo 'Error extract global bdd'
	send_mel 'Extract icompareur: Error extract global bdd'
	exit 1
    fi
    mv "$EMP_SQL_COM_GLOBAL" "$sortie"
    if [ $? -ne 0 ];then
	echo 'Error mv file'
	send_mel 'Extract icompareur: Error mv file'
	exit 1
    fi
} # end fonc_opt_icomparateur_sql

fonc_opt_icomparateur_exec()
{
    func_opt_sql_add_func
    
    rm $EMP_FINAL_ICOM 2>/dev/null
    fonc_opt_icomparateur_sql 0 "$EMP_FINAL_ICOM" 2000

    r=$((2000 - $(wc -l $EMP_FINAL_ICOM | awk '{ print $1}')))
    fonc_opt_icomparateur_sql 1 "$EMP_FINAL_ICOM" $r

    rm $EMP_FINAL_COM_GLOBAL 2>/dev/null
    fonc_opt_comparateur_tout_pro_sql 0 "$EMP_FINAL_COM_GLOBAL" ""
    send_mel 'Extract icompareur: ok'
}

fonc_opt_icomparateur_exec

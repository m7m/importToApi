<?php

// Id de catégories principale ou se trouve les catégories non relié
$id_nontrie = 35;
// Les catégories non relié commence par:
$list_fournisseur = [
    'EC' => 'ECP',
    'AC' => 'ACADIA',
];

// L'utilisateur doit avoir accès au deux base de donnée
// avec au moins les accès SELECT, UPDATE pour prestashop
// et SELECT, UPDATE, INSERT pour la table relationnel

// Nom de la base de donnée relationnel
$db_name = 'coef';
$rel_table = $db_name.'.rel_categories';
$db_host = 'localhost';
$db_port = '3306';
$db_user = 'user';
$db_pass = 'pass';

// Nom de la base de donnée de prestashop
$dbp_name = 'prestashop';

?>

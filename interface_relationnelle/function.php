<?php

require_once('pdebug.php');
require_once('conf.php');

function db_connect() {
    global $db_host, $db_user, $db_pass;
    $db = mysqli_connect($db_host, $db_user, $db_pass);

    if (mysqli_connect_error()) {
        die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
    }
    mysqli_set_charset($db, 'utf8');
    return $db;
}

function q_sql($db, $q) {
    /* pdebug('$q', $q); */
    if ($r = mysqli_query($db, $q))
        return $r;
     die('Connect Error (' . mysqli_connect_errno() . ') '
        . mysqli_connect_error());
}

function getListPrincipal_recursion($db, $q, $id_parent, $nv) {
    global $id_nontrie;

    $sp = '';
    $i = 0;
    while ($i < $nv) {
        $sp .= '&nbsp;';
        $i += 1;
    }

    $stmt = mysqli_stmt_init($db);
    mysqli_stmt_prepare($stmt, $q);
    mysqli_stmt_bind_param($stmt, 'dd', $id_parent, $id_nontrie);
    mysqli_stmt_execute($stmt);
    $r = mysqli_stmt_get_result($stmt);
    while ($rows = mysqli_fetch_array($r)) {
        if ($nv === 0)
            $disable = 'disabled';
        else
            $disable = '';
        printf(
            '<OPTION VALUE="%d" %4$s style="color:#%3$06x">%2$s (%1$d)</OPTION>'.PHP_EOL,
            $rows['id_category'],
            $sp.$rows['name'],
            26850099 >> $nv,
            $disable
            
        );
        getListPrincipal_recursion($db, $q, $rows['id_category'], $nv + 2);
    }
    mysqli_stmt_close($stmt);
}

function getListPrincipal($id_parent = 2, $nv = 0) {
    global $dbp_name;
    $db = db_connect();

    $q = 'SELECT c.id_category, cl.name';
    $q .= ' FROM '.$dbp_name.'.ps_category as c,';
    $q .= ' '.$dbp_name.'.ps_category_lang as cl';
    $q .= ' WHERE c.id_parent = ?';
    $q .= '   AND c.id_category = cl.id_category';
    $q .= '   AND cl.id_lang = 1';
    $q .= '   AND c.id_category != ?';
    $q .= ' ORDER BY cl.name';

    getListPrincipal_recursion($db, $q, 2, 0);
    mysqli_close($db);
}

function getListNonTrie() {
    global $id_nontrie;
    global $dbp_name;
    $db = db_connect();

    $q = 'SELECT c.id_category, cl.name,c.active,';
    $q .= '(SELECT COUNT(*) FROM '.$dbp_name.'.ps_category_product AS cp WHERE cp.id_category = c.id_category) as nb_product';
    $q .= ' FROM '.$dbp_name.'.ps_category as c,';
    $q .= ' '.$dbp_name.'.ps_category_lang as cl';
    $q .= ' WHERE c.id_category = cl.id_category';
    $q .= '  AND c.id_parent = '.$id_nontrie;
    $q .= '  AND cl.id_lang = 1';
    $q .= ' ORDER BY nb_product > 0 DESC, c.active DESC, cl.name ASC';

    $r = q_sql($db, $q);
    foreach ($r as $index) {
        if ($index['active'] == 0)
            $disable = 'disabled';
        else
            $disable = '';
        printf(
            '<OPTION %s VALUE="%d">%s (%2$d)(%4$d)</OPTION>'.PHP_EOL,
            $disable,
            $index['id_category'],
            $index['name'],
            $index['nb_product']
        );
    }
    mysqli_free_result($r);
    mysqli_close($db);
}

function getCheckNonTrei($id_cat) {
    global $id_nontrie;
    global $dbp_name;
    $db = db_connect();

    $q = 'SELECT cl.name';
    $q .= ' FROM '.$dbp_name.'.ps_category as c,';
    $q .= ' '.$dbp_name.'.ps_category_lang as cl';
    $q .= ' WHERE c.id_category = cl.id_category';
    $q .= '  AND c.id_parent = '.$id_nontrie;
    $q .= '  AND c.id_category = ?';
    $q .= '  AND cl.id_lang = 1';
    
    $stmt = mysqli_stmt_init($db);
    mysqli_stmt_prepare($stmt, $q);
    mysqli_stmt_bind_param($stmt, 'd', $id_cat);
    if (!(mysqli_stmt_execute($stmt)))
        die('Failed query.');
    if (!($r = mysqli_stmt_get_result($stmt)))
        die('Failed result.');
    $index = mysqli_fetch_array($r);
    $ret = false;
    if (array_key_exists('name', $index)) {
        $ret = $index['name'];
    }
    mysqli_stmt_close($stmt);
    mysqli_close($db);
    return $ret;
}

function getCheckCat($id_cat) {
    global $id_nontrie;
    global $dbp_name;
    $db = db_connect();

    $q = 'SELECT c.id_parent,cl.name';
    $q .= ' FROM '.$dbp_name.'.ps_category as c,';
    $q .= ' '.$dbp_name.'.ps_category_lang as cl';
    $q .= ' WHERE c.id_category = cl.id_category';
    $q .= '   AND c.id_category = ?';
    $q .= '   AND cl.id_lang = 1';
    $q .= '   AND c.id_parent != '.$id_nontrie;
    
    $stmt = mysqli_stmt_init($db);
    mysqli_stmt_prepare($stmt, $q);
    mysqli_stmt_bind_param($stmt, 'd', $id_cat);
    if (!(mysqli_stmt_execute($stmt)))
        die('Failed query.');

    $ret = false;
    if (!($r = mysqli_stmt_get_result($stmt)))
        die('Failed result.');
    $index = mysqli_fetch_array($r);
    if (array_key_exists('name', $index) && array_key_exists('id_parent', $index)) {
        if ($index['id_parent'] != $id_nontrie)
            $ret = [$index['name'], $index['id_parent']];
    }
    mysqli_stmt_close($stmt);
    mysqli_close($db);
    return $ret;
}

function updateLiason($id_cat, $id_dst, $name_dst, $id_parent) {
    global $dbp_name;

    if (empty($name_dst) || empty($id_parent))
        die('errrrr');
    
    if (!($name_src = getCheckNonTrei($id_cat)))
        die("error: ".$id." ne fait pas partie de la catégies non trié.");
    echo '<b>'.$name_src.'</b>('.$id_cat.') -> <b>'.$name_dst.'</b>('.$id_dst.')<br>';

    $db = db_connect();
    // liste produit pour categorie par defaut
    $q = 'SELECT id_product,reference,ean13,supplier_reference';
    $q .= ' FROM '.$dbp_name.'.ps_product';
    $q .= ' WHERE id_category_default = ?';

    $stmt = mysqli_stmt_init($db);
    mysqli_stmt_prepare($stmt, $q);
    mysqli_stmt_bind_param($stmt, 'd', $id_cat);
    if (!(mysqli_stmt_execute($stmt)))
        die('Failed query.');

    if (!($r = mysqli_stmt_get_result($stmt)))
        die('Failed result.');

    if (mysqli_num_rows($r) > 0) {
        foreach ($r as $index) {
            $p = sprintf(
                "%16s : %16.16s : %16.16s : %s",
                $index['id_product'],
                $index['reference'],
                $index['supplier_reference'],
                $index['ean13']
            );
            echo '<pre>'.$p.'</pre>';
        }

        // deplacement produit pour categorie par defaut
        $q = 'UPDATE '.$dbp_name.'.ps_product SET id_category_default = ? WHERE id_category_default = ?;';
        mysqli_stmt_prepare($stmt, $q);
        mysqli_stmt_bind_param($stmt, 'dd', $id_dst,$id_cat);
        if (!(mysqli_stmt_execute($stmt)))
            die('Failed update.');
        $q = 'UPDATE '.$dbp_name.'.ps_product_shop SET id_category_default = ? WHERE id_category_default = ?';
        mysqli_stmt_prepare($stmt, $q);
        mysqli_stmt_bind_param($stmt, 'dd', $id_dst,$id_cat);
        if (!(mysqli_stmt_execute($stmt)))
            die('Failed update.');
    } else {
        echo '<pre>Aucun produit n\'a cette catégorie par défaut.</pre>';
    }
    
    // liste produit lié par categorie
    $q = 'SELECT cp.id_product,pl.name,p.supplier_reference';
    $q .= ' FROM '.$dbp_name.'.ps_category_product as cp,';
    $q .= ' '.$dbp_name.'.ps_product_lang as pl,';
    $q .= ' '.$dbp_name.'.ps_product as p';
    $q .= ' WHERE cp.id_category = ?';
    $q .= ' AND cp.id_product = p.id_product';
    $q .= ' AND pl.id_product = p.id_product';
    $q .= ' AND pl.id_lang = 1';

    mysqli_stmt_prepare($stmt, $q);
    mysqli_stmt_bind_param($stmt, 'd', $id_cat);
    if (!(mysqli_stmt_execute($stmt)))
        die('Failed query.');
    if (!($r = mysqli_stmt_get_result($stmt)))
        die('erreur lecture de la categorie lié');
    echo "<h4>Liste produit lié à la catégorie: ".$name_src."</h4>";
    foreach ($r as $index) {
        $p = sprintf(
            "%16s : %16.16s : %s",
            $index['id_product'],
            $index['supplier_reference'],
            $index['name']
        );
        echo '<pre>'.$p.'</pre>';
    }
    mysqli_free_result($r);

    // réafectation produit lié a la categorie
    // '.$dbp_name.'.ps_category_product

    echo '<p>';
    echo '<b>Déplacement des produits: </b>';
    $q = 'UPDATE '.$dbp_name.'.ps_category_product';
    $q .= ' SET id_category = ?';
    $q .= ' WHERE id_category = ?';
    mysqli_stmt_prepare($stmt, $q);
    mysqli_stmt_bind_param($stmt, 'dd', $id_dst,$id_cat);
    if (!(mysqli_stmt_execute($stmt)))
        die('Failed update.');
    echo mysqli_stmt_affected_rows($stmt);
    echo '</p>';

    echo '<p>';
    echo '<b>Désactivation de la categorie: </b>';
    $q = 'UPDATE '.$dbp_name.'.ps_category';
    $q .= ' SET active = 0';
    $q .= ' WHERE id_category = ?';
    mysqli_stmt_prepare($stmt, $q);
    mysqli_stmt_bind_param($stmt, 'd', $id_cat);
    if (!(mysqli_stmt_execute($stmt)))
        die('Failed update.');
    echo mysqli_stmt_affected_rows($stmt);
    echo '</p>';

    global $rel_table, $list_fournisseur;

    echo '<p>';
    echo '<b>Insertion de la relation: </b>';
    
    // les deux premières lettre représente le founisseur
    // le fournisseur en minuscule représente le nom de la colonne
    //  dans la table des relation des fournisseur
    $col_name = null;
    foreach ($list_fournisseur as $alias => $supplier_name) {
        $len = mb_strlen($alias);
        if (!strncmp($name_src, $alias.'-', $len + 1)) {
            $supplier = $supplier_name;
            break ;
        }
    }
    if (empty($supplier))
        die('Catégorie source ne correspond pas un fournisseur de la table.');

    $name_src = mb_strcut($name_src, $len + 1);
    
    $q = 'SELECT id';
    $q .= ' FROM '.$rel_table;
    $q .= ' WHERE id_parent = ?';
    $q .= '   AND id_category = ?';
    $q .= '   AND supplier = ?';
    $q .= '   AND category = ?';// ecp = truc/subtruc/... ou acadia = trucacadia/subtrucaca/...

    mysqli_stmt_prepare($stmt, $q);
    $r = mysqli_stmt_bind_param($stmt, 'ddss', $id_parent,$id_dst,$supplier,$name_src);
    if (!$r)
        die('Erreur de parametre:1.');
    if (!(mysqli_stmt_execute($stmt)))
        die('Failed query.');
    if (!($r = mysqli_stmt_get_result($stmt)))
        die('Failed result.');

    if (mysqli_num_rows($r) > 0) {
        die('Association déjà existante.');
    }
    
    $q = 'INSERT INTO '.$rel_table;
    $q .= ' (id_category,id_parent,supplier,category,icecat)';
    $q .= ' VALUES (?,?,?,?,?)';

    mysqli_stmt_prepare($stmt, $q);
    $r = mysqli_stmt_bind_param($stmt, 'ddsss', $id_dst,$id_parent,$supplier,$name_src,$name_dst);
    if (!$r)
        die('Erreur de parametre:2.');
    if (!(mysqli_stmt_execute($stmt)))
        die('Failed query.');
    if (mysqli_stmt_affected_rows($stmt))
        echo 'insert ok.';

    mysqli_stmt_close($stmt);
    mysqli_close($db);
    echo '</p>';
}

function checkIntegrite()
{
    global $rel_table, $dbp_name;
    $q = 'SELECT pc.id_parent,
        r.id_parent AS rel_id_parent,
        pc.id_category,
        r.id_category AS rel_id_cat,
        pcl.name,
        r.category AS rel_name,
         r.supplier
        FROM '.$rel_table.' AS r
        LEFT JOIN '.$dbp_name.'.ps_category AS pc
                 ON pc.id_category = r.id_category
        LEFT JOIN '.$dbp_name.'.ps_category_lang AS pcl
                 ON pcl.id_category = pc.id_category AND pcl.id_lang = 1
        WHERE pcl.name is null
        ORDER BY pc.id_parent, pc.id_category, r.category;';

    $db = db_connect();
    $r = q_sql($db, $q);
    if (mysqli_num_rows($r) > 0) {
        echo '<pre>id_parent:id_category:category:supplier</pre>';
        foreach ($r as $index) {
            echo '<pre>'
                .$index['rel_id_parent']
                .':'.$index['rel_id_cat']
                .':'.$index['rel_name']
                .':'.$index['supplier']
                .'</pre>';
        }
    } else
        echo '<pre>ok</pre>';
}


?>

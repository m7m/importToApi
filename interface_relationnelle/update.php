<?php

if (empty($_POST))
    die('error p');

if (!array_key_exists('id_src', $_POST)
|| !array_key_exists('id_dst', $_POST)
)
    die('error i');
require_once('pdebug.php');

$id_src = $_POST['id_src'];
$id_dst = $_POST['id_dst'];

if (!is_array($id_src))
    $id_src = [$id_src];

foreach ($id_src as $id) {
    if (!is_numeric($id))
        die('Id source invalide.');
}
if (!is_numeric($id_dst))
    die('Id destination invalide.');

require_once('function.php');

if (!(list($name_dst, $id_parent) = getCheckCat($id_dst)))
    die('<pre>error: id categorie de destination est invalide :'.$id_dst.'<pre>');

echo "<h3>Visualisation par catégories:</h3>";
foreach ($id_src as $id) {
    updateLiason($id, $id_dst, $name_dst, $id_parent);
}

$db->close();
?>

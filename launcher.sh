#!/bin/sh

cd "$(dirname $0)"

if [ ! -e 'config/launcher.conf' ];then
	echo 'Fichier de configuration inexistant: config/launcher.conf'
	exit 1
fi
. config/launcher.conf

file_lock="$(pwd)/.lock"
if [ -e $file_lock ];then
    date_file=$(($(stat -c '%Y' $file_lock)+86400))
    date_cur=$(date +'%s')
    if [ ! $date_file -ge $date_cur ];then
		rm $file_lock
    else
		echo 'is lock: '$(stat -c '%y' $file_lock)
		exit 0
    fi
fi
touch $file_lock

exec 1>$FILE_LOG
exec 2>&1

DATE_DEBUT="$(date)"
echo "Débuté à $DATE_DEBUT"
find tmp/ -type f -mtime +7 -delete
find tmp/ -type f -size 0 -delete

send_mel()
{
    SUBJECT="$1"
    if [ -f "$FILE_LOG" ];then
		OPT="-a $FILE_LOG"
    else
		OPT=""
    fi
    echo 'Log ci-joint' \
		| /usr/bin/mailx -s "$SUBJECT" $OPT $ADDRE

    if [ ! -e tmp/recapitulatif.log ];then
		if [ "$(date +'%H')" -ge 22 ] || [ "$DAY" -ne "$(date +'%d')" ];then
			grep -E '^(/tmp|Nombre|Fini)' tmp/$ANN-$MON-${DAY}_*.log \
				 > tmp/recapitulatif.log
			ADDRE="$ADDRE_STAT"
			FILE_LOG=tmp/recapitulatif.log
			send_mel "Rapport d'importation journalier."
			rm tmp/recapitulatif.log
		fi
    fi
}

func_exit()
{
    rm $file_lock
    send_mel "$1"
    exit $2
}

cd $PATH_CATLOGUE;
/usr/bin/python3 launcher.py
if [ $? != 0 ];then
    echo 'Error '$PATH_CATLOGUE
    func_exit "Rapport importation prestashop: ko" 1
fi
cd -

/usr/bin/php launcher.php
if [ $? != 0 ];then
    echo 'Error csvToApi'
    func_exit "Rapport importation prestashop: ko" 1
fi

bash prestashop_postion_alpha.sh
if [ $? != 0 ];then
    echo 'Error trie catégorie'
    func_exit "Rapport importation prestashop: ko" 1
fi

/bin/echo -e "Débuté à $DATE_DEBUT"
/bin/echo -e "Fini à\t $(date)"
func_exit "Rapport importation prestashop: ok" 0

<?php

require 'checkValueXpath.php';
require 'oneQueryXpath.php';

/**
 * @param string $path ex: '/ICECAT-interface/Product/CategoryFeatureGroup[@ID=1]'
 * @param string $value ex: 'Value'
 * @param DOMElement $dom
 * @return checkValueXpath
 */
function oneQueryXpathCheck($xpath, $path, $value, $dom = null)
{
	return (
        checkValueXpath(
            oneQueryXpath($xpath, $path, $dom),
            $value
        )
    );
}

?>

<?php

/**
 * Execution de csvToApi
 *
 * PHP version 7
 *
 * @category Importation
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */

/**
 * Renomme $file_ori en $file_old
 *
 * @param string $file_ori fichier
 * @param string $file_old fichier
 *
 * @return None
 */
function rename_file_in_old($file_ori, $file_old)
{
    if (!rename($file_ori, $file_old))
        die('Move file error: '.$file_ori
            .' to '.$file_old.PHP_EOL);
}

?>
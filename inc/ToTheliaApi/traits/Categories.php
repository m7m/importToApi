<?php

namespace ToTheliaApi;

trait Categories
{
    function select_categories($param)
    {
        
        $n = $this->db->real_escape_string($param['nom']);
        $q = 'SELECT id'
           .' FROM category_i18n as ci'
           .' WHERE ci.title = "'.$n.'"';
        // pdebug('select_categories::$q',$q);
        $id = false;
        if ($r = $this->db->query($q)) {
            // pdebug('select_categories::$r',$r);
            if ($row = $r->fetch_object()) {
                // pdebug('select_product_sale_elements::$row',$row);
                // pdebug('select_product_sale_elements::$row->id',$row->id);
                $id = $row->id;
              //  pdebug('select_categories::$id',$id);
            }
            $r->close();
        }
        return $id;
    }

    
    function getIdCategorie($motif_search)
    {
        $motif_search = ucfirst($motif_search);
        //pdebug('getIdCategorie::$motif_search', $motif_search);
        if (($id = $this->select_categories(
            ['nom'=>$motif_search
             //'locale'=>'fr_FR'
            ]
        )))
            return ($id);
        // si id inexistant nous ajoutons la categories
        $catpost = $this->dataCategories($motif_search);
        // pdebug('getIdCategorie::$catpost', $catpost);
        list($status, $data) = $this->client->doPost('categories', $catpost);
        if ($status !== 201) {
            printf('Erreur sur l'."'".'envoie de la catégorie: %s'.PHP_EOL
                   .'Status: %s'.PHP_EOL, $motif_search, $status);
            return false;
        }
        return $this->getIdCategorie($motif_search);
    }

    function dataCategories($name_cat)
    {
        $catpost = [
            'visible'=> 1,
            'parent'=> $this->default_category_id,
            'locale'=> 'fr_FR',
            'title'=> $name_cat
        ];
        return $catpost;
    }
}
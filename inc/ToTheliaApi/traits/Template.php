<?php

namespace ToTheliaApi;

trait Template
{

    // function insert_feature_template($param)
    // {
    //     $c = implode('`,`', ['feature_id', 'template_id', 'position', 'created_at', 'updated_at']);
    //     $v = implode(',', [$param[0], $this->id_template, 'MAX(position) + 1', 'NOW()', 'NOW()']);
    //     $q = 'INSERT INTO `feature_template`'
    //        .' (`'.$c.'`)'
    //        .' VALUES ('.$v.')';
    //    //  pdebug('insert_feature_template::$q',$q);
    //     if ($q = $this->db->query($q)) {
    //         return true;
    //     }
    //     die ('Erreur sur l'."'".'envoie de la feature_template: '.$param[0].PHP_EOL);
    //     return false;
    // }

    function insert_feature_template($param)
    {
        $table = 'feature_template';
        $max_position = '(SELECT IFNULL(MAX(tmp.position)+1,1) FROM `'.$table.'` as tmp)';
        $q1 = fmt_insert(
            $table,
            ['feature_id', 'template_id', 'position', 'created_at', 'updated_at'],
            [$param[0], $this->id_template, $max_position, 'NOW()', 'NOW()']
        );
        // pdebug('$q1',$q1);
        if ($q = $this->db->query($q1)) {
            return true;
        }
        die ('Erreur sur l'."'".'envoie de la feature_template: '.$param[0].PHP_EOL);
        return false;
    }

    function select_feature_template($param)
    {
        $q = 'SELECT ft.id'
           .' FROM feature_template as ft'
           .' WHERE ft.feature_id = '.$param
           .' AND ft.template_id = '.$this->id_template;
        //pdebug('select_feature_template::$q',$q);

        $id = false;
        if ($r = $this->db->query($q)) {
            if ($row = $r->fetch_object()) {
                $id = $row->id;
              //  pdebug('select_feature_template::$id',$id);
            }
            $r->close();
        }
        return $id;
    }

    function chechAndAddFeatureTemplate($feature_id)
    {
      //  pdebug('chechAndAddFeatureTemplate::','');
        if ($this->select_feature_template($feature_id))
            return true;

        if ($this->sql_queryId(
            "insert_feature_template",
            [$feature_id]))
            return true;
        return false;
    }
}

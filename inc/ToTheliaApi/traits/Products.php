<?php

namespace ToTheliaApi;

trait Products
{

    function getIdProduct($ref = null)
    {
        return $this->getProduct_id($ref);
    }
    
    function getProduct_id($ref = NULL)
    {
        //pdebug('getProduct_id','');
        if ($this->id === 0)
        {
            if (!$ref)
                return false;
            if (!($id = $this->select_product([
                'ref'=>$ref,
            ])))
                return false;

            $this->id = $id;
        }
        return $this->id;
    }

    function sendProd(array $prod)
    {
        if ($this->checkExisteProd($prod))
        {
            echo 'upd';
            //pdebug('sendProd::update','');
            //$this->new = 0;// function datetime isnew = 0|1
            $data_send = $this->dataProdPut($prod);
            list($status, $data) = $this->client->doPut(
                'products', $data_send, $this->getProduct_id());
            if ($status !== 204)
                return false;
        }
        else
        {
            echo 'add';
            //pdebug('sendProd::add','');
            //$this->new = 1;
            $data_send = $this->dataProdPost($prod);
            list($status, $data) = $this->client->doPost('products', $data_send);
            if ($status !== 201)
                return false;
        }
        //pdebug('sendProd::$data', $data);
        //pdebug('sendProd::$status', $status);
        $this->updateProductsCategorieTree($data_send['default_category']);
        return true;
    }
    
    function dataProdPut(array $prod)
    {
        //pdebug('dataProdPut','');
        $default_category = $this->getIdCategorie($prod[INDEX_CATEGORIES]);
        $ref = $this->getRef($prod);
        
        $id_brand = $this->getIdBrand($prod[INDEX_MARQUE]);
        $visible = $this->getVisible();
        $p1 = [
            'ref' => $ref,
            'visible' => $visible,
            'default_category' => $default_category,
            'brand_id'=> $id_brand,
            'title' => $prod[INDEX_LIBELLE_PRODUIT],

            'locale' => 'fr_FR',
            'description' => str_replace('\n','<br>',$prod[INDEX_DESCRIPTIONS_LONG]),
            'postscriptum' => str_replace('\n','<br>',$prod[INDEX_DESCRIPTIONS_COURT]),
        ];
        return $p1;
    }

    function dataProdPost(array $prod)
    {
        //pdebug('dataProdPost','');

        $default_category = $this->getIdCategorie($prod[INDEX_CATEGORIES]);
        // if (!($ref = $prod[INDEX_REF_CONSTRUCTEUR]))
        //     $ref = $prod[INDEX_REF_FOURNISSEUR];
        $ref = $this->getRef($prod);
        $id_brand = $this->getIdBrand($prod[INDEX_MARQUE]);
        $quantity = $this->getQuantity($prod[INDEX_DISPONIBILITE]);
        $visible = $this->getVisible();
        $p1 = [
            'ref' => $ref,
            'default_category' => $default_category,
            'price' => $prod[INDEX_PRIX_HTT],
            'currency' => 1,
            'tax_rule' => 1,
            'weight' => $prod[INDEX_POID],
            'visible' => $visible,

            'locale' => 'fr_FR',
            'title' => $prod[INDEX_LIBELLE_PRODUIT],
            //'chapo' => str_replace('\n','<br>',$prod[INDEX_DESCRIPTIONS_COURT]),
            'description' => str_replace('\n','<br>',$prod[INDEX_DESCRIPTIONS_LONG]),
            'postscriptum' => str_replace('\n','<br>',$prod[INDEX_DESCRIPTIONS_COURT]),

            'quantity' => $quantity,
            'brand_id'=> $id_brand,

            'template_id' => $this->id_template
        ];
        return $p1;
    }

    function select_product($param)
    {
        //pdebug('select_product','');
        $param['ref'] = $this->getRefValid($param['ref']);
        $n = $this->db->real_escape_string($param['ref']);
        $q = 'SELECT id'
           .' FROM product as p'
           .' WHERE p.ref = "'.$n.'"';
        //pdebug('select_product::$q',$q);
        // //pdebug('select_product::$ref','.'.trim($n, ).'.');
        $id = false;
        if ($r = $this->db->query($q)) {
            if ($row = $r->fetch_object())
                $id = $row->id;
            $r->close();
        }
        return $id;
    }

    function select_product_category($id_prod, $id_cat)
    {
        //pdebug('select_product_category','');
        $q = 'SELECT product_id'
           .' FROM product_category'
           .' WHERE product_id = '.$id_prod
           .' AND category_id = '.$id_cat;
        
        $result = false;
        if ($r = $this->db->query($q)) {
            if ($row = $r->fetch_object()) {
                $result = true;
            }
            $r->close();
        }
        return $result;
    }
    
    function insert_product_category($param)
    {
        //pdebug('insert_product_category','');
        $table = 'product_category';
        $max_position = '(SELECT IFNULL(MAX(tmp.position)+1,1)'
                      .' FROM `'.$table.'` as tmp'
                      .' WHERE product_id = '.$param['product_id'].')';

        $r = 'product_id,category_id,default_category,position,created_at,updated_at';
        $q = 'INSERT INTO `'.$table.'` ('. $r .') VALUES';

        $qvalue = "";
        foreach ($param['category_id'] as $id_cat)
        {
            if (!$this->select_product_category($param['product_id'], $id_cat))
            {
                if (!empty($qvalue))
                    $qvalue .= ',';
                $qvalue .= '('.$param['product_id']
                   .', '.$id_cat
                   .', 0, '
                   .$max_position
                   .', NOW(), NOW())';
            }
        }

        if (empty($qvalue))
            return true;
        $q .= $qvalue;
        if ($q = $this->db->query($q))
            return true;
        die ('Erreur sur dans product_category : '.$param[0].PHP_EOL);
        return 0;
    }
    
    function select_categoriesParent($param)
    {
        $q = 'SELECT parent'
           .' FROM category'
           .' WHERE id = '.$param['id'].'';
        
        $id = false;
        if ($r = $this->db->query($q)) {
            if ($row = $r->fetch_object()) {
                $id = $row->parent;
            }
            $r->close();
        }
        //pdebug('select_categoriesParent::$id',$id);
        if ($id === false)
            die('select_categoriesParent::error query, category not found');
        return $id;
    }

    function delete_product_categoryNotdefault()
    {
        $q = 'DELETE FROM `product_category`'
           .' WHERE product_id = '.$this->id
           .' AND default_category = 0';
        $q = $this->db->query($q);
        //die('delete_product_categoryNotdefault::test');
    }
    
    function updateProductsCategorieTree($id_cat)
    {
        //pdebug('updateProductsCategorieTree','');
        $id = $id_cat;
        $tree_id = [];
        //pdebug('updateProductsCategorieTree::$id',$id);
        while (($id = $this->select_categoriesParent(['id'=> $id])) > 0)
        {
            if ($id === $id_cat)
                die('Erreur la categorie ne peux être son propre parent');
            $tree_id = array_merge($tree_id, [$id]);
        }
        if (empty($tree_id))
            return true;
        $this->delete_product_categoryNotdefault();
        if ($this->sql_queryId(
            "insert_product_category",
            [
                'product_id' => $this->id,
                'category_id' => $tree_id
            ]))
            return true;
        return false;
    }
}

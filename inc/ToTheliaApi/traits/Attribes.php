<?php

namespace ToTheliaApi;

trait Attribes
{
    function select_attribute_av($name_supplier)
    {
        $q = 'SELECT aa.id'
           .' FROM `attribute_av` as aa'
           .' LEFT JOIN `attribute_av_i18n` as aai'
           .'  ON aa.id = aai.id'
           .' WHERE aa.attribute_id = '.$this->supplier_id_attribute
           .'  AND aai.title = "'.$name_supplier.'"';
        // pdebug('select_product_sale_elements::$q',$q);
        if ($r = $this->db->query($q)) {
            $id = false;
            if ($row = $r->fetch_object()) {
                // pdebug('select_attribute_av::$row',$row);
                // pdebug('select_attribute_av::$row->id',$row->id);
                $id = $row->id;
            }
            $r->close();
            return $id;
        }
        return false;
    }

    function getIdAttribute_av($name_supplier)
    {
        if ($id = $this->select_attribute_av($name_supplier))
        {
            return [$id];
        } else
            die ('Fournisseur inexistant dans la base de donnée de thelia,'
                 .'veuillez rajouter:'.$name_supplier);
    }
}

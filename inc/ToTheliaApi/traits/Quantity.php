<?php

namespace ToTheliaApi;

trait Quantity
{
    function getQuantity($dispo)
    {
      //  pdebug('getQuantity::dispo', $dispo);
        $q = -1;
        if ($this->visible === 0)
            return 0;
        if (is_numeric($dispo) // && is_int($dispo)
        )
            $q = intval($dispo);
        if ($dispo === 'nd')
            $q = 0;
        if ($dispo === 'l')
            $q = 3;
        if ($dispo === 'd')
            $q = 6;
        return $q;
    }
}

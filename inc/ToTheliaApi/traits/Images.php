<?php

namespace ToTheliaApi;

use DateTime;

trait Images
{

    function checkImagesExiste($id)
    {
        list($status, $data) = $this->client->doList("products/".$id."/images");
        // pdebug('checkImagesExiste::status',$status);
        // pdebug('checkImagesExiste::data',$data);
        if ($status === 200 && !empty($data))
            return true;
        return false;
    }

    // $raw_imgs array ou string
    function sendImages2($ref, $raw_imgs)
    {
        $id = $this->getProduct_Id($ref);
        if (!$this->checkImagesExiste($id))
        {
            $imgs = $this->dataImags($raw_imgs);
            if (!empty($imgs))
            {
                list($status, $data) = $this->client->doPostFormDataImg(
                    'products/'. $id .'/images',
                    $imgs
                );
                if ($status !== 201)
                {
                    echo 'error api:'.$data;
                    return false;
                }
            }
        }
        return true;
    }

    function checkTimeFileCache($filename)
    {
        $date_file = filemtime($filename);
        $date_file_max = (new DateTime())->getTimestamp() - $this->time_cache_sec;
        return ($date_file >= $date_file_max);
    }

    function getImageTmp($img)
    {
        // recuperation temporaire image
        $tmpfile  = $this->tmp .'/imgs/'.basename($img);
        if (file_exists($tmpfile)
            && $this->checkTimeFileCache($tmpfile))
            return $tmpfile;
        $tmpfile_dir = dirname($tmpfile);
        if (!file_exists($tmpfile_dir))
            if (!mkdir($tmpfile_dir, 0777, true))
            {
                echo "Error file access: " . $tmpfile_dir . PHP_EOL;
                return false;
            }
        if (!($filecontent = @file_get_contents($img)))
        {
            echo "Error download file: " . $img . PHP_EOL;
            return false;
        }
        if (!file_put_contents(
            $tmpfile,
            $filecontent))
        {
            echo "Error write file: " . $tmpfile . PHP_EOL;
            return false;
        }
        return realpath($tmpfile);
    }

    function checkExt($filepath)
    {
        if (exif_imagetype($filepath) == IMAGETYPE_PNG)
            $ext = 'png';
        else if (exif_imagetype($filepath) == IMAGETYPE_JPEG)
            $ext = 'jpeg';
        else
            $ext = NULL;
        return $ext;
    }

    function realExt($filepath)
    {
        if (empty($filepath)
            || !($ext = $this->checkExt($filepath)))
            return $filepath;

        $p = pathinfo($filepath);
        if (!strcasecmp($p['extension'], $ext))
            return $filepath;
        $filepathext = $p['dirname'].'/'.$p['filename'].'.'.$ext;
        if (!file_exists($filepathext))
        {
            if (!link($filepath, $filepathext))
            {
                echo 'Error link'.PHP_EOL;
                return NULL;
            }
        }
        return $filepathext;

    }
    
    // $imgs array ou string
    function dataImags($imgs)
    {
        $lst_imgs = [];
        $y = 0;
        $i = 0;
        if (is_array($imgs))
        {
            foreach ($imgs as $img)
            {
                $file = $this->getImageTmp($img);
                $file = $this->realExt($file);
                if ($file) {
                    $lst_imgs['file'.$i] = $file;
                    $y += 1;
                }
                $i += 1;
            }
        } else if (!empty($imgs)) {
            $i = 1;
            $file = $this->getImageTmp($imgs);
            if ($file) {
                $y = 1;
                $lst_imgs[ 'file' ]= $file;
            }
        }
        echo $y.'/'.$i;
        return $lst_imgs;
    }

    function sendImages($prod)
    {
        return $this->sendImages2($this->getRef($prod),$prod[INDEX_IMAGES]);
    }
}

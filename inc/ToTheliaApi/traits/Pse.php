<?php

namespace ToTheliaApi;

trait Pse
{

    // ['nom_fournisseur'=>'ECP', 'id_product'=> getProduct_id()]
    function select_product_sale_elements($param)
    {
        $q = 'SELECT pse.id'
           .' FROM `product_sale_elements` as pse'
           .' LEFT JOIN `attribute_combination` as ac'
           .' ON ac.product_sale_elements_id = pse.id'
           .' LEFT JOIN `attribute_av_i18n` as aai'
           .' ON ac.attribute_av_id = aai.id'
           .' WHERE pse.product_id='.$param['id_product']
           .' AND aai.title = "'.$param['nom_fournisseur'].'"';
        // pdebug('select_product_sale_elements::$q',$q);
        if ($r = $this->db->query($q)) {
            $id = false;
            if ($row = $r->fetch_object()) {
                // pdebug('select_product_sale_elements::$row',$row);
                // pdebug('select_product_sale_elements::$row->id',$row->id);
                $id = $row->id;
            }
            $r->close();
            return $id;
        }
        return false;
    }

    function getIdPse(array $prod)
    {
        $id = $this->select_product_sale_elements(
            [
                'nom_fournisseur' => $prod[INDEX_FOURNISSEUR],
                'id_product' => $this->getProduct_id($this->getRef($prod))
            ]
        );
      //  pdebug('getIdPse::$id',$id);
        return $id;
    }

    function sendPse(array $prod)
    {
        if ($this->supplier_id_attribute !== 0) {
            if ($this->checkExisteProd($prod))
            {

                if ($id = $this->getIdPse($prod)) {//put
                    echo 'upd';
                    $data_pse = $this->dataPse_update($prod, $id);
                    list($status, $data) = $this->client->doPut('pse', $data_pse);
                } else {
                    echo 'add';
                    $data_pse = $this->dataPse_add($prod);
                    list($status, $data) = $this->client->doPost('pse', $data_pse);
                }
              //  pdebug('sendPse::$status',$status);
                //pdebug('sendPse::$data',$data);
                if ($status === 201)
                    return true;
                printf('Erreur récupération de la liste,' .PHP_EOL);
                printf('Status: %d' .PHP_EOL, $status);
            }
          //  pdebug('sendPse::pse_id',$this->pse_id);
        }
    }
    
    // seulement en post
    function dataPse_add(array $prod)
    {
        return [ 'pse' => [[
            'product_id' => $this->getProduct_id(),
            'ean_code' => $prod[INDEX_EAN_CODE],
            'reference' => $prod[INDEX_REF_FOURNISSEUR],

            'isnew' => $this->getIsnew(),
            'onsale' => false,
            
            'price' => $prod[INDEX_PRIX_HTT],
            'quantity' => 1,
            'currency_id' => 1,
            'tax_rule_id' => 1,
            'weight' => 2,
            'isdefault' => 1,

            'attribute_av'=> $this->getIdAttribute_av($prod[INDEX_FOURNISSEUR]),
            // 'description' => str_replace('\n','<br>',$prod[INDEX_DESCRIPTIONS_LONG]),
            // 'postscriptum' => str_replace('\n','<br>',$prod[INDEX_DESCRIPTIONS_COURT])

        ]]];
    }
    
    function dataPse_update(array $prod, $pse_id)
    {
        return [ 'pse' => [[
            'product_id' => $this->id,
            'id' => $pse_id,

            'ean_code' => $prod[INDEX_EAN_CODE],
            'isnew' => $this->getIsnew(),//optionnel

            'price' => $prod[INDEX_PRIX_HTT],
            'quantity' => $this->getQuantity($prod[INDEX_DISPONIBILITE]),
            'onsale' => false,
            'attribute_av'=> $this->getIdAttribute_av($prod[INDEX_FOURNISSEUR]),
            // 'description' => str_replace('\n','<br>',$prod[INDEX_DESCRIPTIONS_LONG]),
            // 'postscriptum' => str_replace('\n','<br>',$prod[INDEX_DESCRIPTIONS_COURT])

        ]]];
    }

}

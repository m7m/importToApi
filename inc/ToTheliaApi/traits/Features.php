<?php

namespace ToTheliaApi;

trait Features
{
    function updatePosition()
    {

    }
    
    function select_feature($param)
    {
        $n = $this->db->real_escape_string($param['nom']);
        $l = $this->db->real_escape_string($param['locale']);
        $q = 'SELECT id'
           .' FROM feature_i18n as fi'
           .' WHERE fi.title = "'.$n.'"'
           .' AND fi.locale = "'.$l.'"';
        // pdebug('select_feature::$q',$q);
        $id = false;
        if ($r = $this->db->query($q)) {
            // pdebug('select_feature::$r',$r);
            if ($row = $r->fetch_object()) {
                // pdebug('select_product_sale_elements::$row',$row);
                // pdebug('select_product_sale_elements::$row->id',$row->id);
                $id = $row->id;
              //  pdebug('select_feature::$id',$id);
            }
            $r->close();
        }
        return $id;
    }

    function insert_feature_i18n($id, $param)
    {
        $q2 = fmt_insert_fk(
            'feature_i18n',
            ['id', 'locale', 'title'],
            array_fill(0, 3, '?'),
            'feature'
        ) . ' WHERE id ='.$id;
        
        $type = 'dss';
        $args = [
            &$type,
            &$id,
            &$param[1],
            &$param[0]
        ];
        $r = query_stmt($this->db, $q2, $args);
        if (!$r)
            return false;
        return true;
    }

    function insert_feature($param)
    {
        $table = 'feature';
        $max_position = '(SELECT IFNULL(MAX(tmp.position)+1,1) FROM `'.$table.'` as tmp)';
        $q1 = fmt_insert(
            $table,
            ['visible', 'position', 'created_at', 'updated_at'],
            [$param[2], $max_position, 'NOW()', 'NOW()']
        );
        // pdebug('$q1',$q1);
        if ($q = $this->db->query($q1)) {
            $id = $this->db->insert_id;
            if ($this->insert_feature_i18n($id, $param))
                return $id;
            $this->db->query('DELETE FROM feature WHERE id='.$id);
        }
        die ('Erreur sur l'."'".'envoie de la feature: '.$param[0].PHP_EOL);
    }
    
    function getIdFeature($feature_familly)
    {
        if (($id = $this->select_feature([
            'nom'=>$feature_familly,
            'locale'=>'fr_FR'
            ])))
            return ($id);

        if ($this->sql_queryId(
            'insert_feature',
            [$feature_familly, 'fr_FR',1]))
            return $this->getIdFeature($feature_familly);

    }

    function select_feature_av($param)
    {
        $n = $this->db->real_escape_string($param['nom']);
        $l = $this->db->real_escape_string($param['locale']);
        $q = 'SELECT fa.id'
           .' FROM feature_av_i18n as fai'
           .' LEFT JOIN feature_av as fa'
           .'  ON fa.id = fai.id'
           .' LEFT JOIN feature as f'
           .'  ON fa.feature_id = '.$param['feature_id']
           .' WHERE fai.title = "'.$n.'"'
           .' AND fai.locale = "'.$l.'"';
         // pdebug('select_feature_av::$q',$q);
        $id = false;
        if ($r = $this->db->query($q)) {
            // pdebug('select_feature::$r',$r);
            if ($row = $r->fetch_object()) {
                // pdebug('select_product_sale_elements::$row',$row);
                // pdebug('select_product_sale_elements::$row->id',$row->id);
                $id = $row->id;
              //  pdebug('select_feature_av::$id',$id);
            }
            $r->close();
        }
        
        return $id;
    }

    function insert_feature_av_i18n($id, $param)
    {
        $q2 = fmt_insert_fk(
            'feature_av_i18n',
            ['id', 'locale', 'title'],
            array_fill(0, 3, '?'),
            'feature_av'
        ) . ' WHERE id ='.$id;
        
        $type = 'dss';
        $args = [
            &$type,
            &$id,
            &$param[1],
            &$param[0]
        ];
        //pdebug('insert_feature_av_i18n::$q',$q2);
        $r = query_stmt($this->db, $q2, $args);
        if (!$r)
            return false;
        return true;
    }

    function insert_feature_av($param)
    {
        $q1 = fmt_insert_fk(
            'feature_av',
            ['feature_id', 'position', 'created_at', 'updated_at'],
            [$param[2], 'IFNULL(MAX(position) + 1,1)', 'NOW()', 'NOW()'],
            'feature'
        );
        // pdebug('insert_feature_av::$q1',$q1);
        if ($q = $this->db->query($q1)) {
            $id = $this->db->insert_id;
            if ($this->insert_feature_av_i18n($id, $param))
                return $id;
            $this->db->query('DELETE FROM feature_av WHERE id='.$id);
        }
        die ('Erreur sur l'."'".'envoie de la feature_av: '.$param[0].PHP_EOL);
    }
    
    function getIdFeature_Av($feature_id, $feature_av)
    {
        if (strlen($feature_av) >= 255)
        {
            $feature_av = substr($feature_av, 0, 253);
            $feature_av[252] = '.';
            $feature_av[251] = '.';
        }
        
        if (($id = $this->select_feature_av([
            'feature_id' => $feature_id,
            'nom'=>$feature_av,
            'locale'=>'fr_FR'
            ])))
            return ($id);

        if ($this->sql_queryId(
            'insert_feature_av',
            [$feature_av, 'fr_FR',$feature_id]))
            return $this->getIdFeature_Av($feature_id, $feature_av);

    }

    function select_feature_product($param)
    {
        $q = 'SELECT fp.id'
           .' FROM feature_product as fp'
           .' WHERE fp.product_id = '.$param['product_id']
           .' AND fp.feature_id = '.$param['feature_id']
           .' AND fp.feature_av_id = '.$param['feature_av_id'];
        //pdebug('select_feature_product::$q',$q);
        $id = false;
        if ($r = $this->db->query($q)) {
            //pdebug('select_feature_product::$r',$r);
            if ($row = $r->fetch_object()) {
                //pdebug('select_product_sale_elements::$row',$row);
                // pdebug('select_product_sale_elements::$row->id',$row->id);
                $id = $row->id;
              //  pdebug('select_feature_product::$id',$id);
            }
            $r->close();
        }
        return $id;
    }


    function insert_feature_product($param)
    {
        // $v = implode(',', [$param['product_id'], $param['feature_id'], $param['feature_av_id'], 'NOW()', 'NOW()']);
        // $q = 'INSERT INTO `feature_product`'
        //    .' (`product_id`, `feature_id`, `feature_av_id`, `position`, `created_at`, `updated_at`)'
        //    .' VALUES ('.$v.')';

        $table = 'feature_product';
        $max_position = '(SELECT IFNULL(MAX(tmp.position)+1,1) FROM `'.$table.'` as tmp)';
        $q = fmt_insert(
            $table,
            ['product_id', 'feature_id', 'feature_av_id', 'position', 'created_at', 'updated_at'],
            [$param['product_id'], $param['feature_id'], $param['feature_av_id'], $max_position, 'NOW()', 'NOW()']
        );

        //pdebug('insert_feature_product::$q',$q);
         
        if ($this->db->query($q))
            return true;
        die ('Erreur sur l'."'".'envoie de la feature_product: '.$param[0].PHP_EOL);
        return false;
    }
    
    function checkAndAddFeature($id_product, $feature_id, $fai)
    {
        //pdebug('sendFeature::array::$fai',$fai);
        $feature_av_id = $this->getIdFeature_Av($feature_id, $fai);
        $param = [
            'product_id' => $id_product,
            'feature_id' => $feature_id,
            'feature_av_id' => $feature_av_id
        ];
        //pdebug('checkAndAddFeature::$param',$param);
        if (!($id = $this->select_feature_product($param)))
        {
          //  pdebug('checkAndAddFeature::$id',$id);
            if ($this->sql_queryId('insert_feature_product', $param))
                if ($this->chechAndAddFeatureTemplate($param['feature_id']))
                    return true;
            return perror('Error check and add feature:'
                          .$id_product
                          .':'.$feature_id
                          .':'.$feature_av_id);
        }
        return true;
    }

    function sendFeature(array $prod)
    {
        //pdebug('sendFeature::$prod',$prod);
        if (!array_key_exists(INDEX_CARACTERISTIQUE, $prod)
            || !is_array($prod[INDEX_CARACTERISTIQUE]))
            return perror('Not feature');
        if (array_key_exists(INDEX_CARACTERISTIQUE_LANG, $prod))
            $l = $prod[INDEX_CARACTERISTIQUE_LANG];
        else
            $l = 'null';
        $id_product = $this->getProduct_id($this->getRef($prod));
        if (!$id_product)
            return perror('Product id not found');
        foreach ($prod[INDEX_CARACTERISTIQUE] as $feature_familly => $feature)
        {

            // // feature titre
            // $feature_familly = trim($feature_familly);
            // $feature_familly_id = $this->getIdFeature($feature_familly);
            // //  pdebug('sendFeature::$feature_familly_id',$feature_familly_id);
            // // if (!$this->checkAndAddFeature($id_product, $feature_familly_id, ' '))
            // //     return false;

            foreach ($feature as $feature_i18n => $feature_av_i18n)
            {
                // feature
                $fi =  str_replace('\n','<br>',$feature_i18n);
                $fi = trim($fi);
                $feature_id = $this->getIdFeature($fi);
                // feature_av
                $fai = str_replace('\n',',',$feature_av_i18n);
                $fai = trim($fai);
                $fais = explode(',', $fai);
                //  pdebug('sendFeature::array::$fais',$fais);
                if (!is_array($fais))
                    $fais = [$fais];
                foreach ($fais as $fai)
                    if (!$this->checkAndAddFeature($id_product, $feature_id, $fai))
                        return false;
            }
        }
        return true;
    }
}

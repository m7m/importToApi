<?php

namespace ToTheliaApi;

trait Brands
{
    function insert_brand($param)
    {
        $table = 'brand';
        $max_position = '(SELECT IFNULL(MAX(tmp.position)+1,1) FROM `'.$table.'` as tmp)';
        $q1 = fmt_insert(
            $table,
            ['visible', 'position', 'created_at', 'updated_at'],
            [$param[2], $max_position, 'NOW()', 'NOW()']
        );
        // pdebug('insert_brand::$q1',$q1);
        if ($q = $this->db->query($q1)) {
            $id = $this->db->insert_id;
            if ($this->insert_brand_i18n($id, $param))
                return $id;
            $this->db->query('DELETE FROM brand WHERE id='.$id);
        }
        die ('Erreur sur l'."'".'envoie de la marque: '.$param[0].PHP_EOL);
        return 0;
    }

    function insert_brand_i18n($id, $param)
    {
        $q2 = fmt_insert_fk(
            'brand_i18n',
            ['id', 'locale', 'title'],
            array_fill(0, 3, '?'),
            'brand'
        ) . ' WHERE id ='.$id;
        
        $type = 'dss';
        $args = [
            &$type,
            &$id,
            &$param[1],
            &$param[0]
        ];
        $r = query_stmt($this->db, $q2, $args);
        if (!$r)
            return false;
        return true;
    }

    function select_brand($param)
    {
        $n = $this->db->real_escape_string($param['title']);
        $l = $this->db->real_escape_string($param['locale']);
        $q = 'SELECT id'
           .' FROM brand_i18n as b'
           .' WHERE b.title = "'.$n.'"'
           .' AND b.locale = "'.$l.'"';
        // pdebug('select_feature::$q',$q);
        $id = false;
        if ($r = $this->db->query($q)) {
            // pdebug('select_feature::$r',$r);
            if ($row = $r->fetch_object()) {
                // pdebug('select_product_sale_elements::$row',$row);
                // pdebug('select_product_sale_elements::$row->id',$row->id);
                $id = $row->id;
              //  pdebug('select_feature::$id',$id);
            }
            $r->close();
        }
        return $id;
    }

    function getIdBrand($motif_search)
    {
        $title = ucfirst($motif_search);
        if (($id = $this->select_brand([
            'title'=>$title,
            'locale'=>'fr_FR'
            ])))
            return ($id);

        // pdebug('getIdBrand::$id',$id);
        // pdebug('getIdBrand::$title',$title);
        if ($this->sql_queryId(
            "insert_brand",
            [$title, 'fr_FR', 1]))
            return $this->getIdBrand($title);
        return (0);
    }

}
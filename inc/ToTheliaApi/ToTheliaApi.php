<?php

namespace ToTheliaApi;

require 'vendor/autoload.php';
require 'sql.php';
require 'traits/Brands.php';
require 'traits/Categories.php';
require 'traits/Products.php';
require 'traits/Pse.php';
require 'traits/Quantity.php';
require 'traits/Attribes.php';
require 'traits/Images.php';
require 'traits/Features.php';
require 'traits/Template.php';

require_once 'ToBase/ToBase.php';

use ToBase\ToBase;
use Thelia\Api\Client\Client;

function perror($msg)
{
    echo $msg;
    return false;
}

class ToTheliaApi extends ToBase
{
    use Brands;
    use Categories;
    use Products;
    use Pse;
    use Quantity;
    use Attribes;
    use Images;
    use Features;
    use Template;
    
    /**
     * @param string $apiToken
     * @param string $apiKey
     * @param string $apiUri
     */
    function __construct($apiToken, $apiKey, $apiUri)
    {
        $this->client = new Client(
            $apiToken,
            $apiKey,
            $apiUri
        );
        $this->new = 0;
        $this->visible = 0;
        $this->id = 0;
        $this->pse_id = 0;

        $this->default_category_id = 0;
        $this->id_template = 1;
        $this->supplier_id_attribute = 0;
        $this->tmp = 'tmp';
        $this->time_cache_sec = 2592000;

        
        $this->db = NULL;// new mysqli(...)
    }

    function __destruct()
    {
        if ($this->db)
        {
            printf('Close database.'.PHP_EOL);
            $this->db->close();
        }
    }

    function getList(
        &$this_list,
        $func_get_list = 'listCategories',//function api
        $force = false)
    {
        if ($this_list === NULL || $force)
        {
            list($status, $data) = $this->client-> $func_get_list();
            if ($status === 200)
                $this_list = $data;
            else
            {
                printf('Erreur récupération de la liste,' .PHP_EOL);
                printf('Status: %d' .PHP_EOL, $status);
                return false;
            }
            //pdebug('getList::status', $status);
            // pdebug('getList::this_list', $this_list);
        }
        return $this_list;
    }
    
    function getSearchId(
        $motif_search,
        &$this_list,
        $func_get_list = listCategories,//function api
        $force = false)
    {
        $c = false;
        if ($list = $this->getList(
            $this_list,
            $func_get_list
        ))
        {
            foreach ($list as $l)
            {
                if ($l['TITLE'] === $motif_search)
                {
                    $c = $l['ID'];
                    // check value is valide
                    break;
                }
            }
            //pdebug('getSearchId::motif_search',$motif_search);
            //die ('Not found');
        }
        //pdebug('getSearchId::$c',$c);
        return $c;
    }
    
    /**
     * Fonction de autre chose
     */
    function getVisible()
    {
        $this->visible = 1;
        return $this->visible;
    }
    
    function sql_queryId($func_insert_table, $param)
    {
        // client api thelia incomplet, passage par la base de donnée
        if (!$this->db)
            die ('$this->db is NULL.');
        if ($this->db->connect_error) {
            die ('Error connect sql');
        }
        
        $id = $this-> $func_insert_table($param);
        if (!$id)
            return false;
        return $id;
    }

    function getIsnew()
    {
        return $this->new;
    }

    function getIsvisible()
    {
        return $this->visible;
    }

    function pstatus($func_sendXxxx, $msg, $prod)
    {
        echo $msg.':';
        if ($this-> $func_sendXxxx($prod))
            $status = "\033[32mok\033[0m";
        else
            $status = "\033[31mko\033[0m";
        echo ' '.$status.' ';

    }
    
    /**
     * @param array $prod =
     * 00 Fournisseur
     * 01 ean_code
     * 02 ref_constructeur
     * 03 ref_fournisseur
     * 04 libelle_produit
     * 05 marque
     * 06 categories ([cat1, cat2, ...])
     * 07 prix_htt
     * 08 ecotax
     * 09 sacem
     * 10 copie_privee
     * 11 disponibilite (nd(non disponible), l(limit) or d(disponble)) / quantité
     * 12 poid
     * 13 images ([img1,img2,...])
     * 14 descriptions_court
     * 15 descriptions_long
     * 16 caracteristique ([[...]])
     * 17 visible
     * 18 nouveau
     * 19 descriptions_court_lang
     * 20 descriptions_long_lang
     * 21 libelle_produit_lang
     * 22 categories_lang
     * 23 images_lang
     * 24 caracteristique_lang
     */
    function ligne_CsvArrayToArrayApi(array $prod)
    {
        $this->id = 0;

        // sendProd call:
        //  getIdCategorie, getIdBrand, getQuantity, getVisible, getRef,

        //  sendFeature, sendPse, sendImages
        // echo $prod[INDEX_REF_FOURNISSEUR]
        //     .':'
        //     .$prod[INDEX_REF_CONSTRUCTEUR]
        //     .':';

        $this->pstatus('sendProd', 'Prod', $prod);
        $this->pstatus('sendFeature', 'Features', $prod);
        $this->pstatus('sendPse', 'Pse', $prod);
        $this->pstatus('sendImages', 'Images', $prod);
        echo PHP_EOL;
        
        // $prod[8],// ecotax
        // $prod[9],// sacem
        // $prod[10],// copie prive
    }

    function send(array $prod)
    {
        $this->ligne_CsvArrayToArrayApi($prod);
    }
}

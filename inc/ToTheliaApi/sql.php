<?php

/**
 * @param string $table
 * @param array $row
 * @param array $value
 * @param string $from
 * @return string
 * formate la chaine pour une requete sql
 */
function fmt_insert($table, array $row, array $value)
{
    $r = implode("`,`", $row);
    $v = implode(",", $value);
    
    $query = 'INSERT INTO `'.$table.'` (`'. $r .'`)'
           .' VALUES ('. $v .')';
    // pdebug('fmt_insert::$query',$query);
    return $query;

}

/**
 * @param string $table
 * @param array $row
 * @param array $value
 * @param string $from
 * @return string
 * formate la chaine pour une requete sql avec clé étrangère
 */
function fmt_insert_fk($table, array $row, array $value, $from)
{
    $r = implode("`,`", $row);
    $v = implode(",", $value);
    
    $query = 'INSERT INTO `'.$table.'` (`'. $r .'`)'
        .' SELECT '. $v .''
        .' FROM `'.$from.'`';
    return $query;

}

/**
 * @param mysqli $db
 * @param string $query
 * @param array $args = [&$v1, &$v2, &$v3,...]
 * @return false|true
 * Prepare une requete sql puis l'execute, retourne sont état
 *  INSERT INTO `brand_i18n` (`id`,`locale`,`title`) 
 *  SELECT ?,?,?
 *  FROM `brand` WHERE id = 1
 *  args = [1, value2, value3 ]
 * execute:
 *  INSERT INTO `brand_i18n` (`id`,`locale`,`title`) 
 *  SELECT 1,value2,value3
 *  FROM `brand` WHERE id = 1
 */
function query_stmt($db, $query, $args, $close=true)
{
    $r = false;
    $stmt = $db->stmt_init();
    if ($stmt->prepare($query)) {

        $r = call_user_func_array (
            [$stmt, 'bind_param'],
            $args
        );


        //$r = $stmt->bind_param('dss', $v1, $v2, $v3);
        if ($r)
            $r = $stmt->execute();
        // $t = $stmt->bind_result($v);
        // pdebug('sql::$t',$t);
        // pdebug('sql::$stmt->fetch()',$stmt->fetch());
        // pdebug('sql::$v',$v);
        // var_dump($r);
        // printf('%d ligne insérée.'.PHP_EOL, $stmt->affected_rows);
    }
    if ($close)
    {
        $stmt->close();
        return $r;
    }
    return $stmt;
}

function sql_query($db, array $func_query, $param)
{
    if (!$db || !($db instanceof mysqli))
        die ('$db n est pas une instance de mysqli');
    if ($db->connect_error) {
        die ('Error connect sql');
    }
    $value = $func_query($param);
    if (!$value)
        return false;
    return $value;
}

?>

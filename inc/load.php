<?php
set_include_path(get_include_path() . PATH_SEPARATOR . 'inc');

require 'FileCache.php';

require 'Icecat/XmlIcecat.php' ;
require 'DiffFile.php';

require 'oneQueryXpathCheck.php';

require 'pdebug.php';
require 'rename_file_in_old.php';

require 'ToBase/ToBase.php';

require 'ToPrestashop/ToPrestashop.php';
require 'ToTheliaApi/ToTheliaApi.php';

require 'config/CsvToApi.conf.php';
require 'CsvToApi/CsvToApi.php';

require 'RelFourIcecat/RelFourIcecat.php';

?>

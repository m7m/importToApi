<?php

namespace ToBase;

class ToBase
{
    
    function getRefValid($ref)
    {
        $ref = str_replace("\xC2\xA0", ' ', $ref);
        $ref = trim($ref);
        return $ref;
    }
    
    function getRef(array $prod)
    {
        if (!($ref = $prod[INDEX_REF_CONSTRUCTEUR]))
            if (!($ref = $prod[INDEX_REF_FOURNISSEUR]))
            {
                die('Reference is nil');
            }
        return $this->getRefValid($ref);
    }

    // /**
    //  * @param array $prod fiche produit
    //  *
    //  * @return true|false
    //  */
    // function checkExisteProd(array $prod)
    // {
    //     //pdebug('checkExisteProd','');
    //     /* if ($this->getIdProduct($prod[INDEX_REF_CONSTRUCTEUR]) === false) */
    //         if (!$this->getIdProductSupplier($prod[INDEX_REF_FOURNISSEUR]))
    //             return false;
    //     return true;
    // }

    /**
     * @param array $prod fiche produit
     *
     * @return true|false
     */
    function checkExisteProd(array $prod)
    {
        $existRefSupplier = $this->getIdProductSupplier($prod[INDEX_REF_FOURNISSEUR]);
        $this->id = 0;
        $existRef = $this->getIdProduct($prod[INDEX_REF_CONSTRUCTEUR]);

        /* pdebug('$prod[INDEX_REF_FOURNISSEUR', $prod[INDEX_REF_FOURNISSEUR]); */
        /* pdebug('$prod[INDEX_REF_CONSTRUCTEUR', $prod[INDEX_REF_CONSTRUCTEUR]); */
        
        /* pdebug('existRef',$existRef); */
        /* pdebug('existRefSupplier',$existRefSupplier); */
        if ($existRef) {
            if (!$existRefSupplier) {
                // produit existe donc add product_suppliers
                $this->getIdSupplier($prod[INDEX_FOURNISSEUR]);
                /* pdebug('$this->cur_id_supplier::',$this->cur_id_supplier); */
                /* pdebug('$this->id_supplier::',$this->id_supplier); */
                if ($this->getCheckProductSupplier($this->id_supplier, $this->id)) {
                    // erreur référence constructeur differente pour un même fournisseur, bug: icecat ?
                    echo 'erreur référence fournisseur non trouvé pour un produit existant et qui a déjà une référence pour ce constructeur:';
                    return -1;
                }
                else if ($this->cur_id_supplier != $this->id_supplier)
                    $this->prodSupplierAdd($prod[INDEX_REF_FOURNISSEUR]);
            }
            return true;
        }
        return false;
    }

}

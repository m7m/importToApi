<?php

namespace Icecat\XmlIcecat;

use DomDocument;
use DomXpath;
use DateTime;

interface iXmlIcecat
{
    function checkTimeFileCache($filename);
    function getXmlIcecat($icecat_path, $filecache = NULL);
    function getXmlProductByEanUpc($ean_upc, $brand);
    function extractInfoProductXpath(array $elements, array $product = array(NULL));
    function retArrayElements($name, $path, $value);
    function getArrayExtractInfoProductXpath();
    function getNameGroupFeatureById($catFeatId);
    function getIdGroupFeatureByLocalValue($domElemLocalValue);
    function extractInfoProductFeatureXpath();
    function getProductByEanUpc($ean_upc, $brand, array $product = [NULL]);

    function getRefConstructeur();
    function getProductName();
    function getTitle();
    function getCategories();
    function getBrand();
    function getDescLong();
    function getDescPdf();
    function getDescUrl();
    function getDescWarranty();
    function getDescShort();
    function getFeatures();
    function getImages();
}

class XmlIcecat implements iXmlIcecat
{

    /**
     * @param $icecat_user = "user_icecat";
     * @param $icecat_pass = "pass_icecat";
     * @param $icecat_uri = "data.icecat.biz";
     * @param $icecat_lang = "FR"
     * @param $tmp = 'tmp' cache
     * @param int $time_cache_sec = 86400
     *
     */
    public function __construct(
        $icecat_user,
        $icecat_pass,
        $icecat_uri,
        $icecat_lang,
        $icecat_lang_default = 'EN',
        $tmp = 'tmp',
        $time_cache_sec = 9986400
    )
    {
        $this->icecat_url = 'https://'
                    . $icecat_user
                    . ':' . $icecat_pass
                    . '@' . $icecat_uri;
        $this->lang = $icecat_lang;
        $this->lang_default = $icecat_lang_default;
        $this->tmp = $tmp;
        $this->time_cache_sec = $time_cache_sec;

        $this->error = 0;
        $this->not_found = 0;
        $this->full_icecat = 0;
        $this->invalide = 0;
        $this->valide = 0;
        $this->total = 0;
    }

    /**
     * Affiche un recapitulatif
     */
    function __destruct()
    {
        printf(
            'Récapitulatif Icecat:'
            .' error %d, not found %d, full icecat %d, invalide %d, valide %d, nb query %d'
            .PHP_EOL,

            $this->error,
            $this->not_found,
            $this->full_icecat,
            $this->invalide,
            $this->valide,
            $this->total
        );
    }
            
    
    /**
     * @param string $filename
     * @return bool
     */
    function checkTimeFileCache($filename)
    {
        $date_file = filemtime($filename);
        $date_file_max = (new DateTime())->getTimestamp() - $this->time_cache_sec;
        return ($date_file >= $date_file_max);
    }

    /**
     * @param string $icecat_path exemple: export/freexml.int/FR/1431.xml
     * @param string $filecache
     * @return false|DOMDocument
     * Recuperer le fichier sur icecat ou si il existe en local charge celui-ci
     * Utiliser sur les fiches produit
     */
    function getXmlIcecat($icecat_path, $filecache = NULL) {

        $dom = new DomDocument('1.0', 'UTF-8');

        $icecat_filecache = $this->tmp . '/' . $this->lang . '/';
        if ($filecache)
            $icecat_filecache .= $filecache;
        else
            $icecat_filecache .= $icecat_path;
        // pdebug('$icecat_filecache', $icecat_filecache);
        // pdebug('getXmlIcecat::file_exists($icecat_filecache)', file_exists($icecat_filecache));
        if (file_exists($icecat_filecache)
            && $this->checkTimeFileCache($icecat_filecache))
            echo 'C';
        else
        {
            // ajout test si variable valide
            $icecat_urlpath = $this->icecat_url . '/' . $icecat_path;
            echo 'D';
            if (!$filetmp = file_get_contents($icecat_urlpath))
            {
                echo 'Error file access: ' . $icecat_urlpath;
                return false;
            }

            $icecat_pathname = dirname($icecat_filecache);
            if (!file_exists($icecat_pathname)
                && !mkdir($icecat_pathname, 0777, true))
            {
                echo 'Error file access: ' . $icecat_pathname;
                return false;
            }
            if (!file_put_contents($icecat_filecache, $filetmp))
            {
                echo 'Error write file: ' . $icecat_filecache;
                return false;
            }
        }
        $dom->load($icecat_filecache);
        return $dom;
    }

    /**
     * @param $ean_upc
     * @return false|DOMNodeList
     */
    function getXmlProductByEanUpc($ean_upc, $brand)
    {
        if (!is_numeric($ean_upc))
        {
            echo "ean not valide";
            $this->error += 1;
            return false;
        }
        if (empty($brand))
        {
            echo "Brand is empty";
            $this->error += 1;
            return false;
        }

        $xml_ean = 'xml_s3/xml_server3.cgi?'
                 . 'ean_upc=' . $ean_upc
                 . ';supplier=' . urlencode($brand)
                 . ';lang=' . $this->lang
                 . ';output=productxml';
        $dom = $this->getXmlIcecat(
            $xml_ean, 'ean/' . $ean_upc . '.xml', $this->lang);
        //pdebug('$dom', $dom);

        if ($dom) {
            $this->xpath = new DomXpath($dom);
            // check is valide
            $ean_upcQuery = $this->xpath->query(
                '/ICECAT-interface/Product[@Code="1"]'
            );
            //pdebug('$ean_upcQuery', $ean_upcQuery);
            if ($ean_upcQuery->length === 1)
            {
                $this->dom = $dom;
                $this->valide += 1;
                return $ean_upcQuery;
            }
            $q = $this->xpath->query(
                '/ICECAT-interface/Product[@Code="-1"][@ErrorMessage="The requested XML data-sheet is not present in the Icecat database."]'
            );
            if ($q->length === 1) {
                echo 'not present';
                $this->not_found += 1;
            } else {
                $q = $this->xpath->query(
                    '/ICECAT-interface/Product[@Code="-1"][@ErrorMessage="You are not allowed to have Full ICEcat access"]'
                );
                if ($q->length === 1) {
                    echo 'Full ICEcat';
                    $this->full_icecat += 1;
                } else {
                    echo 'error';
                    $this->error += 1;
                }
            }
        } else {
            echo 'Not valide xml';
            $this->error += 1;
        }
        return false;
    }

    /**
     * @param array $elements
     * @param array $product utiliser pour la recursion
     * @return array $product|NULL
     */
    function extractInfoProductXpath(array $elements, array $product = array(NULL))
    {
        foreach ($elements as $e)
        {
            $q = oneQueryXpath($this->xpath, 
                '/ICECAT-interface/Product/' . $e['path']
            );
            if (!array_key_exists($e['name'], $product) || empty($product[ $e['name'] ]))
            {
                $product[ $e['name'] ] = checkValueXpath($q, $e['value']);

                if (!empty($product[ $e['name'] ])
                    && (!array_key_exists($e['name'] . '_lang', $product)
                        || !$product[ $e['name'] . '_lang']))
                    $product[ $e['name'] . '_lang'] = $this->lang;
            } 
        }
        // pdebug('icecat::$product',$product);
        return $product;
    }

    /**
     * @param string $name
     * @param string $path
     * @param string $value
     * @return array
     */
    function retArrayElements($name, $path, $value)
    {
        return [
            'name' => $name,
            'path' => $path,
            'value' => $value
        ];
    }

    /**
     * @return array
     */
    function getArrayExtractInfoProductXpath()
    {
        return (
            [
                $this->retArrayElements('Product', '.', 'Name'),
                $this->retArrayElements('Title', '.', 'Title'),
                $this->retArrayElements('Categories', 'Category/Name', 'Value'),
                $this->retArrayElements('Brand', 'Supplier', 'Name'),
                $this->retArrayElements('Ref', '.', 'Prod_id'),

                $this->retArrayElements('DescLong', 'ProductDescription', 'LongDesc'),
                $this->retArrayElements('DescPdf', 'ProductDescription', 'PDFURL'),
                $this->retArrayElements('DescUrl', 'ProductDescription', 'URL'),
                $this->retArrayElements('DescWarranty', 'ProductDescription', 'WarrantyInfo'),
                $this->retArrayElements('DescShort', 'ProductDescription', 'ShortDesc'),
            ]
        );
    }

    /**
     * @param string $catFeatId
     * @return oneQueryXpathCheck|NULL
     */
    function getNameGroupFeatureById($catFeatId)
    {
        if ($catFeatId)
        {
            return oneQueryXpathCheck($this->xpath,
                '/ICECAT-interface/Product/CategoryFeatureGroup[@ID='. $catFeatId .']'
                .'/FeatureGroup/Name',
                'Value'
            );
        }
        return NULL;
    }

    /**
     * @param DOMElement $domElemLocalValue
     * @return oneQueryXpathCheck
     */
    function getIdGroupFeatureByLocalValue($domElemLocalValue)
    {
        return oneQueryXpathCheck($this->xpath,
            '..',
            'CategoryFeatureGroup_ID',
            $domElemLocalValue
        );
    }

    /**
     * @return array $listFeature|NULL
     */
    function extractInfoProductFeatureXpath()
    {
        /**
         * Categories de la caracteristique:
         * "/Product/ProductFeature[@CategoryFeatureGroup_ID]" == "digit" == $pfCefid
         * "/Product/CategoryFeatureGroup[@ID=". $pfCefid ."]/FeatureGroup/Name[@Value]"
         * 
         * Caracteristique:
         * Nom:
         * /Product/ProductFeature/Feature/Name[@Value] == Type de commutateur
         * Valeur:
         * /Product/ProductFeature/LocalValue[@Value] == "Managed network switch"
         */

        $listFeature = $this->xpath->query(
            '/ICECAT-interface/Product/ProductFeature'
        );

        $f = NULL;
        foreach ($listFeature as $feature)
        {
            $n = oneQueryXpathCheck($this->xpath,
                'Feature/Name', 'Value', $feature);
            if ($n)
            {
                $elem = oneQueryXpath($this->xpath, 'LocalValue', $feature);

                $catFeatId = $this->getIdGroupFeatureByLocalValue($elem);
                $cat = $this->getNameGroupFeatureById($catFeatId);
                if (!$cat)
                    $cat = 'None';

                $v = checkValueXpath($elem, 'Value');
                $f[$cat][$n] = $v.' '.trim($elem->nodeValue);

            }
        }
        return ($f);
    }

    function extractImagesToXpath()
    {
        $q_imgs = $this->xpath->query(
            '/ICECAT-interface/Product/ProductGallery/ProductPicture'
        );

        $imgs = NULL;
        $i = 0;
        foreach ($q_imgs as $img)
        {
            $v = checkValueXpath($img, 'Original');
            // pdebug('$v', $v);
            if ($v)
            {
                $imgs[$i] = $v;
                $i++;
            }

        }
        return ($imgs);
    }
    
    /**
     * @param string $ean_upc
     * @param strinf $brand
     * @param array $product utiliser pour la recursion
     * @return array $produit|NULL
     */
    function getProductByEanUpc($ean_upc, $brand, array $product = [])
    {
        $this->total += 1;
        $this->product = NULL;
        if ($this->getXmlProductByEanUpc($ean_upc, $brand)) {
            $product = $this->extractInfoProductXpath(
                $this->getArrayExtractInfoProductXpath(),
                $product
            );
            if (!array_key_exists('Features', $product)) {
                $product['Features'] = $this->extractInfoProductFeatureXpath();
                $product['Features_lang'] = $this->lang;
            }
            if (!array_key_exists('Imgs', $product))
                $product['Imgs'] = $this->extractImagesToXpath();

            // Information missing search in lang_default
            if ($this->lang !== $this->lang_default)
            {
                $user_lang = $this->lang;
                $this->lang = $this->lang_default;
                $product = $this->getProductByEanUpc($ean_upc, $brand, $product);
                $this->lang = $user_lang;
            }
        } else
            return false;
        $this->product = $product;
        return $product;
    }

    function checkValue(array $p, $v)
    {
        if (empty($p) || empty($v) || !is_array($p))
            return false;
        if (!array_key_exists($v, $p))
            return false;
        if (empty($p[$v]))
            return false;
        if (array_key_exists($v . '_lang', $p))
            return [ $p[$v], $p[$v . '_lang'] ];
        return [$p[$v], NULL];
    }
    
    function getRefConstructeur()
    {
        return $this->checkValue($this->product, 'Ref');
    }

    function getProductName()
    {
        return $this->checkValue($this->product, 'Product');
    }

    function getTitle()
    {
        return $this->checkValue($this->product, 'Title');
    }

    function getCategories()
    {
        return $this->checkValue($this->product, 'Categories');
    }

    function getBrand()
    {
        return $this->checkValue($this->product, 'Brand');
    }

    function getDescLong()
    {
        return $this->checkValue($this->product, 'DescLong');
    }

    function getDescPdf()
    {
        return $this->checkValue($this->product, 'DescPdf');
    }

    function getDescUrl()
    {
        return $this->checkValue($this->product, 'DescUrl');
    }

    function getDescWarranty()
    {
        return $this->checkValue($this->product, 'DescWarranty');
    }

    function getDescShort()
    {
        return $this->checkValue($this->product, 'DescShort');
    }

    function getFeatures()
    {
        return $this->checkValue($this->product, 'Features');
    }
    function getFeaturesLang()
    {
        return $this->checkValue($this->product, 'Features_lang');
    }

    function getImages()
    {
        return $this->checkValue($this->product, 'Imgs');
    }
}

// $o = new XmlIcecat(
//     "XXXXX",
//     "xxxxx",
//     "data.icecat.biz",
//     "FR"
// );
// //$o->time_cache_sec = 30;

// // //$EAN = "1330381404719";
// // //$EAN = "0731304272908";
// // //$EAN = "4713392496233";//icecat sans description
// $EAN = "0889523009734";//GV-N108TGAMING OC-11GD desciption en englais
// $BRAND = "Gigabyte";

// // $EAN = "606449069983";// complete fr
// // $BRAND = "Netgear";
// // //$EAN = "1234";

// $product = $o->getProductByEanUpc($EAN, $BRAND);
// // pdebug('product', $product);

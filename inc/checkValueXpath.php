<?php

/**
 * @param DOMElement $obj
 * @param string $value
 * @return string|NULL
 */
function checkValueXpath($obj, $value)
{
    if ($obj && $obj->hasAttribute($value))
        return $obj->getAttribute($value);
    return null;
}

?>

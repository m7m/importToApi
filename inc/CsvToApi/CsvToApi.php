<?php

/**
 * Execution de csvToApi
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */

namespace CsvToApi;

// require 'pdebug.php';
// require 'ToTheliaApi/ToTheliaApi.php';
// require 'ToPrestashop/ToPrestashop.php';
// require 'CsvToApi_constant.php';
// require 'DiffFile.php';
// require 'rename_file_in_old.php';

require 'ICsvToApi.php';

use mysqli;
use ICsvToApi;
use Icecat\XmlIcecat\XmlIcecat;
use ToTheliaApi\ToTheliaApi;
use ToPrestashop\ToPrestashop;
use DiffFile;

/**
 * Execution de csvToApi
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */
class CsvToApiOverIcecat implements ICsvToApi
{
    public static $only_icecat = 0;
    public static $tmp_path = null;
    /**
     * Initialise la classe XmlIcecat et celle de l'api($func_api)
     *
     * @param string $icecat_user user icecat
     * @param string $icecat_pass password  icecat
     * @param string $icecat_uri  uri icecat, exemple = data.icecat.biz
     * @param string $icecat_lang lang icecat, exemple = EN, FR, ...
     * @param string $file_csv    file csv
     * @param string $apiToken    Token api
     * @param string $apiKey      Key api
     * @param string $apiUri      Uri api,
     *                            exemple = http://ip.dns:port/thelia/web/index.php
     * @param string $func_api    Class api, default = "ToTheliaApi\ToTheliaApi"
     *
     * @return None
     */
    function __construct(
        $icecat_user,
        $icecat_pass,
        $icecat_uri,
        $icecat_lang,
        $file_csv,
        $apiToken,
        $apiKey,
        $apiUri,
        $func_api = "ToTheliaApi\ToTheliaApi",
        $tmp_path = null
    ) {
        /* if (!is_readable($file_csv)) { */
        /*     die('File not readable: '.$file_csv. PHP_EOL); */
        /* } */
        
        $this->icecat = new XmlIcecat(
            $icecat_user,
            $icecat_pass,
            $icecat_uri,
            $icecat_lang
        );
        $this->fapi = new $func_api(
            $apiToken,
            $apiKey,
            $apiUri);
        $this->file_csv = $file_csv;

        // connecteur mysqli à la base de donnée contenant la table coef
        // permettant d'appliqué un coeffiscient en fonction d'une tranche de prix
        // min;max;coef
        $this->db_coef = null;

        // connecteur mysqli à la base de donnée contenant la table relation_categories_icecat
        // permet la liaison entre catégorie fournisseur et la catégorie icecat
        // voir : sql/relation_categories_icecat.sql
        $this->db_relfi = null;
        
        $this->movefile = true;
        self::$tmp_path = $tmp_path;
    }

    /**
     * Retourne une description par defaut
     *
     * @param array $prod fiche produit
     *
     * @return string
     */
    function getDescLongDefault(array $prod)
    {
        return $prod[INDEX_DESCRIPTIONS_LONG];
    }

    /**
     * Retourne format de description court par defaut
     *
     * @param array $n Name
     * @param array $v Value
     *
     * @return string
     */
    function fmtDescShortDefault($n, $v)
    {
        return ('<tr>'
                . '<td>'. $n . ':<td>'
                . '<td>'. $v . '<td>'
                . '</tr>');
    }
    
    /**
     * Retourne une description court par defaut
     *
     * @param array $prod fiche produit
     *
     * @return string
     */
    function getDescShortDefault(array $prod)
    {
        return null;
        $desc = '<table cellspacing="0" cellpadding="5" width="100%">'
            .  '<tbody>'
            .   $this->fmtDescShortDefault('Marque', $prod[INDEX_MARQUE])
            .   $this->fmtDescShortDefault(
                'Reference constructeur',
                $prod[INDEX_REF_CONSTRUCTEUR]
            )
            .   $this->fmtDescShortDefault('Code EAN', $prod[INDEX_EAN_CODE])
            .   $this->fmtDescShortDefault('Poids brut', $prod[INDEX_POID])
            .   $this->fmtDescShortDefault(
                'Copie privée',
                $prod[INDEX_COPIE_PRIVEE]
            )
            .   $this->fmtDescShortDefault('Eco tax', $prod[INDEX_ECOTAX])
            .   $this->fmtDescShortDefault(
                'Prix hors taxe',
                $prod[INDEX_PRIX_HTT] . ' htt*'
            )
            .  '</tbody>'
            . '</table>'
            . '<br>*Copie privée inclus'
        ;
        return $desc;
    }

    /**
     * Appel $func_get de la class XmlIcecat
     *
     * @param array $func_get  function de la class XmlIcecat
     * @param none  $prod_case pointeur sur une zone de la fiche produit ($prod)
     * @param none  $lang      pointeur sur une zone de la fiche produit ($prod)
     *
     * @return None
     */
    function mergeIcecatGet($func_get, &$prod_case, &$lang = null)
    {
        list($t, $l) = $this->icecat-> $func_get();
        if ($t) {
            $prod_case = $t;
            if ($lang) {
                $lang = $l;
            }
        }
    }

    /**
     * Vérifie la référence constructeur venant du fournisseur
     * et de icecat
     * @return true|false (true si différent)
     */
    function checkRefFourIcecat($refSupplier, $refIcecat) {
        /* $s = [' ', '-', '/', '#', '_', '&', '=']; */
        /* $refSupplier = str_replace($s, '', $refSupplier); */
        /* $refIcecat = str_replace($s, '', $refIcecat); */

        $refSupplier = preg_replace(
            '/(((#[A-Z0-9]{3}|\/(SEE|ELS|FR|EN|E|EU|EUR))$)| |-|\/|#|_|&|=)/m',
            '',
            $refSupplier
        );

        $refIcecat = preg_replace(
            '/(((#[A-Z0-9]{3}|\/(SEE|ELS|FR|EN|E|EU|EUR))$)| |-|\/|#|_|&|=)/m',
            '',
            $refIcecat
        );

        if (strcasecmp($refSupplier, $refIcecat))
            return true; // ko
        return false;
    }
    
    /**
     * Fusion les informations de icecat avec une ligne du fichie csv
     *
     * @param array $prod fiche produit venant du csv
     *
     * @return true|false
     */
    function mergeIcecat(array &$prod)
    {

        if ($this->icecat->getProductByEanUpc(
            $prod[INDEX_EAN_CODE], $prod[INDEX_MARQUE]
        )) {
            $ref = trim($prod[INDEX_REF_CONSTRUCTEUR]);
            $this->mergeIcecatGet(
                "getRefConstructeur",
                $prod[INDEX_REF_CONSTRUCTEUR]
            );
            if ($this->checkRefFourIcecat($ref, $prod[INDEX_REF_CONSTRUCTEUR])) {
                echo ':les références différe:'.$prod[INDEX_REF_CONSTRUCTEUR];
                return false;
            }
            
            $this->mergeIcecatGet(
                "getTitle",
                $prod[INDEX_LIBELLE_PRODUIT],
                $prod[INDEX_LIBELLE_PRODUIT_LANG]
            );
            $this->mergeIcecatGet(
                "getCategories",
                $prod[INDEX_CATEGORIES_ICECAT],
                $prod[INDEX_CATEGORIES_ICECAT_LANG]
            );
            $this->mergeIcecatGet(
                "getDescLong",
                $prod[INDEX_DESCRIPTIONS_LONG],
                $prod[INDEX_DESCRIPTIONS_LONG_LANG]
            );
            $this->mergeIcecatGet(
                "getDescShort",
                $prod[INDEX_DESCRIPTIONS_COURT],
                $prod[INDEX_DESCRIPTIONS_COURT_LANG]
            );
            $this->mergeIcecatGet(
                "getFeatures",
                $prod[INDEX_CARACTERISTIQUE]
            );
            $this->mergeIcecatGet(
                "getFeaturesLang",
                $prod[INDEX_CARACTERISTIQUE_LANG]
            );
            $this->mergeIcecatGet(
                "getImages",
                $prod[INDEX_IMAGES],
                $prod[INDEX_IMAGES_LANG]
            );
            
            $this->icecat->product = null;
            return true;
        }
        return false;
    }

    /**
     * Intérroge la base de donnée et retourne le coeffisient
     * 
     * @param array $param = ['prix' => (int)]
     *
     * @return int
     */
    function selectCoef($param)
    {
        $q = 'SELECT coef'
           .' FROM coef'
           .' WHERE min <= '.$param['prix']
           .' AND max >= '.$param['prix'];
        $coef = false;
        if ($r = $this->db_coef->query($q)) {
            if ($row = $r->fetch_object()) {
                $coef = $row->coef;
            }
            $r->close();
        }
        return $coef;
    }
    
    /**
     * Calcule le prix en fonction du coeffisient et de la tax
     * 
     * @param int $prix prix hors tax du produit
     * @param int $tax  culmule des taxes
     *
     * @return None
     */
    function coef(&$prix, $tax)
    {
        $func = [$this, 'selectCoef'];
        $p = sql_query($this->db_coef, $func, ['prix'=>$prix]);
        if (!$p) {
            die('Le prix ne correspond a aucune tranche de coef:'.$prix);
        }
        $prix = $prix * $p + $tax;
    }

    function fixCharact(array &$prod)
    {
        // $t = &$prod[INDEX_DESCRIPTIONS_LONG];
        // $t = str_replace('\n','<br>', $t);
        // $t = &$prod[INDEX_DESCRIPTIONS_COURT];
        // $t = str_replace('\n','<br>', $t);

        $t = [
            &$prod[INDEX_DESCRIPTIONS_LONG],
            &$prod[INDEX_DESCRIPTIONS_COURT]
        ];
        $i = 0;
        while ($i < 2) {
            $t[$i] = str_replace('\n','<br>', $t[$i]);
            $i += 1;
        }
        $t = &$prod[INDEX_CARACTERISTIQUE];
        if (is_array($t))
            foreach ($t as $k => $v) {
                $k = str_replace('\n','<br>', $k);
                $v = str_replace('\n','<br>', $v);
                $tnew_sub = [];
                foreach ($v as $kv => $vv) {
                    $kv = str_replace('\n','<br>', $kv);
                    $vv = str_replace('\n','<br>', $vv);
                    $tnew_sub[$kv] = $vv;
                }
                if (is_array($v))
                    $tnew[$k] = $tnew_sub;
                else
                    $tnew[$k] = $v;
            }
        else
            $tnew = str_replace('\n','<br>', $t);
        $prod[INDEX_CARACTERISTIQUE] = $tnew;
    }

    public static $force = [
        'AMD',
        'ASUS',
        'ASROCK',
        'COOLMASTER',
        'COOL MASTER',
        'EVGA',
        'SYNOLOGY',
        'LG',
        'TRANSCEND',
        'INFOSEC',
        'TP-LINK',
        'SAPPHIRE',
        'FOXCONN',
        'MSI',
        'HITACHI',
        'CRUSIAL',
        'DELL',
        'PNY',
        'APPLE/MAC',
        'ANTEC',
        'RAZER'
    ];

    function forceImport(array $line) {
        if (!strcasecmp($line[INDEX_FOURNISSEUR], 'ALSO')
        || !strcasecmp($line[INDEX_FOURNISSEUR], 'ECP')) {
            foreach (self::$force as $f) {
                if (!strcasecmp($line[INDEX_MARQUE], $f))
                    return false;
            }
        }
        return true;
    }
    
    /**
     * Traitement d'une ligne du fichier csv
     * 
     * @param int   $i    index sur le fichier csv, valeur optionnel
     * @param array $line Ligne du fichier csv
     *
     * @return None
     */
    function sendLine(&$i, $line)
    {
        $i += 1;
        if ($line) {
            $line[19] = 'fr';

            printf(
                '%05d: %-16.16s: %-16.16s : %16.16s :',
                $i,
                $line[INDEX_REF_FOURNISSEUR],
                $line[INDEX_REF_CONSTRUCTEUR],
                $line[INDEX_EAN_CODE]
            );
            //icecat
            echo 'icecat ';
            if ($this->mergeIcecat($line))
                $status = "ok";
            else {
                $status = "ko";
                if (self::$only_icecat && $this->forceImport($line)) {
                    echo PHP_EOL;
                    return ;
                }
            }
            echo ' '.$status.' ';

            if (empty($line[INDEX_DESCRIPTIONS_LONG]))
                $line[INDEX_DESCRIPTIONS_LONG] = $this->getDescLongDefault($line);
            if (empty($line[INDEX_DESCRIPTIONS_COURT]))
                $line[INDEX_DESCRIPTIONS_COURT] = $this->getDescShortDefault($line);

            $line['SUPPLIER_PRIX_HTT'] = $line[INDEX_PRIX_HTT];
            if ((float)$line[INDEX_PRIX_HTT] == 0.0) {
                $visible = $this->fapi::$visible;
                $this->fapi::$visible = 0;
            } else
                $visible = 0;
            // application du coef et des tax
            $this->coef(
                $line[INDEX_PRIX_HTT],
                $line[INDEX_ECOTAX]
                + $line[INDEX_SACEM]
                + $line[INDEX_COPIE_PRIVEE]
            );
            $this->fixCharact($line);

            $p = $this->fapi->send($line);
            if ($visible) {
                $this->fapi::$visible = $visible;
                $visible = 0;
            }
        }
        else
            printf('Error csv ligne: %d' . PHP_EOL, $i);
    }

    function move($file_src, $file_dst) {
        if (self::$tmp_path)
            $file_dst = self::$tmp_path.'/'.basename($file_dst);
        rename_file_in_old($file_src, $file_dst);
    }
    /**
     * Send transmet la fiche produit à l'api
     *
     * @return None
     */
    function send()
    {
        echo $this->file_csv.PHP_EOL;
        if (!file_exists($this->file_csv))
            die('Error file inexists:'.$this->file_csv);
        if ($f = fopen($this->file_csv, 'r')) {
            $i = 0;
            while (($line = fgetcsv($f, 131072, ';')) != false)
            {
                if (!empty($line) && count($line) > 1) {
                    $this->sendLine($i, $line);
                }
            }
            fclose($f);
            if ($this->movefile)
                $this->move($this->file_csv, $this->file_csv . '.old');
        } else
            printf('Error open file : %s' . PHP_EOL, $this->file_csv);
    }

    /**
     * Update vérifie si le csv d'entrée et different 
     *  puis trait la différence en conséquence
     *
     * @return None
     */
    function update()
    {
        if (!is_readable($this->file_csv))
            die('File not access:'.$this->file_csv);
        echo $this->file_csv.':update'.PHP_EOL;

        $file_ori = $this->file_csv;
        if (self::$tmp_path)
            $this->file_csv = self::$tmp_path.'/'.basename($this->file_csv);
        $file_old = $this->file_csv . '.old';
        $file_dif = $this->file_csv . '.diff';
        $file_del = $this->file_csv . '.del';
        $file_new = $this->file_csv . '.new';

        if (!file_exists($file_old)) {
            echo 'Add all'.PHP_EOL;
            $this->file_csv = $file_ori;
            $this->send();
            return ;
        }
        $this->movefile = false;// s'effectue a la fin sans vérification
        
        $diffFile = new DiffFile(
            $file_ori,
            $file_old,
            $file_dif,
            $file_del,
            $file_new
        );
        $diffFile->exec();
        
        if (filesize($file_dif) != 0) {
            echo 'Maj product:'.$diffFile->maj.PHP_EOL;
            $this->file_csv = $file_dif;
            $this->send();
        }
        if (filesize($file_del) != 0) {
            echo 'Disable product:'.$diffFile->del.PHP_EOL;
            $this->file_csv = $file_del;
            $this->fapi::$visible = 0;
            $this->send();
        }
        if (filesize($file_new) != 0) {
            echo 'Add product:'.$diffFile->new.PHP_EOL;
            $this->file_csv = $file_new;
            $this->fapi::$visible = 1;
            $this->fapi::$new = 1;
            $this->send();
        }
        
        echo 'Nombre de produit nouveau :   '.$diffFile->new.PHP_EOL;
        echo 'Nombre de produit desactivé:  '.$diffFile->del.PHP_EOL;
        echo 'Nombre de produit mise à jour:'.$diffFile->maj.PHP_EOL;

        $this->move($file_ori, $file_old);
    }

}

// $o = new csvToApiOverIcecat(
//     "user",
//     "password",
//     "data.icecat.biz",
//     "FR",

//     //"test.csv",

//     "token",
//     "key",
//     "http://localhost:80/thelia_test/web/index.php",

//     "ToTheliaApi\ToTheliaApi"
// );

// // thelia api incomplete
// $o->coef_db = new mysqli(
//     "localhost",
//     "root",
//     "pasword",
//     "coef"
// );
// $o->fapi->db = new mysqli(
//     "localhost",
//     "root",
//     "password",
//     "thelia_test"
// );

// $o->fapi->db->set_charset('utf8');
// $o->fapi->id_template = 1;// gabarie par defaut
// $o->fapi->supplier_id_attribute = 1; // id fournisseur dans gabarit->attribut
// $o->fapi->default_category_id = 1; // categorie parent par defaut
// $TMP = '/media/stage/tmp';
// $o->icecat->tmp = $TMP;
// $o->fapi->tmp = $TMP;
// $o->movefile = false; // test
// $o->send();
// //$o->update();

?>

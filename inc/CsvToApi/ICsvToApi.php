<?php

/**
 * Interface CsvToApi
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */
interface ICsvToApi
{
    /**
     * Send transmet la fiche produit à l'api
     *
     * @return None
     */
    function send();
    /**
     * Update vérifie si le csv d'entrée et different 
     *  puis trait la différence en conséquence
     *
     * @return None
     */
    function update();
}

<?php

//require "CsvToApi_constant.php";

function test_diff($ou, $in, $index)
{
    return ($ou[$index] == $in[$index]);
}

class DiffFile
{
    /**
     * Les fichier $file_csv_in et $file_csv_ou doivent être trié sur la clé fournisseur
     */
    function __construct(
        $file_csv_in,
        $file_csv_ou,
        $file_csv_diff,
        $file_csv_del,
        $file_csv_new
    )
    {
        
        
        if (!($this->f_in = fopen($file_csv_in, 'r')))
            die('error open file in');

        if (!($this->f_ou = fopen($file_csv_ou, 'r')))
            fclose($this->f_in) && die('error open file ou');

        if (!($this->f_diff = fopen($file_csv_diff, 'w'))) {
            fclose($this->f_ou) ; fclose($this->f_in) ;
            die('error open file diff');
        }

        if (!($this->f_del = fopen($file_csv_del, 'w'))) {
            fclose($this->f_ou) ; fclose($this->f_in) ; fclose($this->f_diff);
            die('error open file del');
        }

        if (!($this->f_new = fopen($file_csv_new, 'w'))) {
            fclose($this->f_ou) ; fclose($this->f_in) ; fclose($this->f_diff);
            fclose($this->f_del);
            die('error open file new');
        }

        $this->new = 0;
        $this->del = 0;
        $this->maj = 0;
        $this->next('f_in', 'in');
        $this->next('f_ou', 'ou');
    }

    function next($f, $inou)
    {
        $ret = fgetcsv($this-> $f, 0, ';');
        if ($ret == FALSE || empty($ret)) {
            $ret = NULL;
        } else if (empty($ret[0])) {
            $ret = NULL;
        }
        $this-> $inou = $ret;
    }

    function checkRef()
    {
        if (empty($this->ou))
            die('err donnée invalide $this->ou');
        if (empty($this->in))
            die('err donnée invalide $this->in');

        if (empty($this->ou[INDEX_REF_FOURNISSEUR]))
            $iou = INDEX_REF_CONSTRUCTEUR;
        else
            $iou = INDEX_REF_FOURNISSEUR;
        if (empty($this->in[INDEX_REF_FOURNISSEUR]))
            $iin = INDEX_REF_CONSTRUCTEUR;
        else
            $iin = INDEX_REF_FOURNISSEUR;

        if (empty($this->ou[$iou])) {
            die('err reference invalide $this->ou[$i]:'.$iou);
        }
        if (empty($this->in[$iin])) {
            die('err reference invalide $this->in[$i]:'.$iin);
        }
        return [$iou, $iin];
    }

    function cmp()
    {
        list($iou, $iin) = $this->checkRef();
        return (strcmp($this->in[$iin], $this->ou[$iou]));
    }

    function add_new()
    {
        // new
        $this->new += 1;
        fputcsv($this->f_new, $this->in, ';');
        $this->next('f_in', 'in');
    }       

    function del_old()
    {
        // delete $ou
        $this->del += 1;
        fputcsv($this->f_del, $this->ou, ';');
        $this->next('f_ou', 'ou');
    }
    
    function test_diffstr()
    {
        $d = $this->cmp();
        if ($d < 0) // in < ou
            $this->add_new();

        else if ($d == 0)
        {
            //maj
            if (!test_diff($this->ou, $this->in, INDEX_PRIX_HTT)
                || !test_diff($this->ou, $this->in, INDEX_DISPONIBILITE))
            {
                // maj
                $this->maj += 1;
                fputcsv($this->f_diff, $this->in, ';');
            }
            $this->next('f_in', 'in');
            $this->next('f_ou', 'ou');
        }

        else // in > ou
            $this->del_old();
    }

    function exec()
    {
        while ($this->in && $this->ou)
            $this->test_diffstr();
        while ($this->in)
            $this->add_new();
        while ($this->ou)
            $this->del_old();
        fclose($this->f_ou);
        fclose($this->f_in);
        fclose($this->f_diff);
        fclose($this->f_del);
    }
}

// $file_csv_in = 't1.csv';
// //$file_csv_in = 'py_FournisseurToCsvStandard/tmp/01_produit.csv.sort';
// $file_csv_ou = 't2.csv';
// $file_csv_diff = 't.diff';
// $file_csv_del = 't.del';
// $file_csv_new = 't.new';

// $o = new DiffFile(
//     $file_csv_in,
//     $file_csv_ou,
//     $file_csv_diff,
//     $file_csv_del,
//     $file_csv_new
// );

// $o->exec();
// echo 'maj:'.$o->maj.PHP_EOL;
// echo 'del:'.$o->del.PHP_EOL;
// echo 'new:'.$o->new.PHP_EOL;

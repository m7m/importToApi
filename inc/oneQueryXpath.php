<?php

/**
 * @param string $path ex: '/ICECAT-interface/Product/CategoryFeatureGroup[@ID=1]'
 * @param DOMElement $dom
 * @return DOMElement|NULL
 */
function oneQueryXpath($xpath, $path, $dom = null)
{
    if ($dom)
        $q = $xpath->query($path, $dom)->item(0);
    else
        $q = $xpath->query($path)->item(0);
    return ($q);
}

?>

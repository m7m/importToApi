<?php


/**
 * Gestionnaire de relation des catégories fournisseurs avec icecat
 */

namespace RelFourIcecat;

/**
 * La valeur Icecat est inutile
 */

class relFourIcecat
{
    public static $table = 'rel_categories';
    public $db = null;
    function __construct($host, $user, $pass, $database) {
        $this->db = new \mysqli(
            $host,
            $user,
            $pass,
            $database
        );
        if ($this->db->connect_error) {
            die('Connect Error (' . $this->db->connect_errno . ') '
                . $this->db->connect_error);
        }
        mysqli_set_charset($this->db, 'utf8');
    }
    function __destruct() {
        if ($this->db)
            mysqli_close($this->db);
    }

    function exec_q($q, $error_fatal = true) {
        if (!($r = $this->db->query($q))) {
            if ($error_fatal) {
                die('Error (' . $this->db->connect_errno . ') '
                    . $this->db->connect_error);
            }
            return false;
        }
        return $r;
    }

    function getRelIdCategory($iaa) {
        if (empty($iaa['category'])) {
            die('category is empty.');
        }
        if (empty($iaa['supplier'])) {
            die('supplier is empty.');
        }
        $q = 'SELECT id_category FROM '.self::$table;
        $q .= ' WHERE category = "'.$iaa['category'].'"';
        $q .= '   AND supplier = "'.$iaa['supplier'].'"';
        $id = false;
        if ($r = $this->exec_q($q, false)) {
            $v = $r->fetch_array();
            if (mysqli_num_rows($r) > 1) {
                $id = -1;
                pdebug('category', $iaa['category']);
                pdebug('supplier', $iaa['supplier']);
                foreach ($r as $v) {
                    pdebug('$v',$v);
                }
                die('Relation inter<->fournisseur en erreur, voir la bdd.');
            }
            else if (!empty($v))
                $id = $v['id_category'];
            $r->close();
        }
        return $id; // id|false|-1
    }

    function checkExiste($iaa) {
        // pdebug('->checkExiste');
        $q = 'SELECT id_category FROM '.self::$table;
        $q .= ' WHERE category = "'.$iaa['category'].'"';
        $q .= ' AND supplier = "'.$iaa['supplier'].'"';
        $q .= ' AND id_parent = '.$iaa['id_parent'];
        $q .= ' AND id_category = '.$iaa['id_category'];
        $r = $this->exec_q($q, false);
        $ret = false;
        if ($r && ($nb = mysqli_num_rows($r)) > 0) {
            $v = $r->fetch_array();
            if (!empty($v))
                $ret = $nb;
            $r->close();
        }
        return $ret; // true|false
    }
    
    function add_q($iaa) {
        if (!array_key_exists('id_category', $iaa)) {
            die('id_category'.' is not set in array.');
        }
        if (!array_key_exists('id_parent', $iaa)) {
            die('id_parent'.' is not set in array.');
        }
        if (!array_key_exists('supplier', $iaa)
        || empty($iaa['supplier'])) {
            die('supplier'.' is not set in array.');
        }

        $q = 'INSERT INTO '.self::$table;
        $q .= ' (id_category,id_parent,category,supplier)';
        $q .= ' VALUES(';
        $q .= $iaa['id_category'].',';
        $q .= $iaa['id_parent'].',';
        $q .= '"'.$iaa['category'].'",';
        $q .= '"'.$iaa['supplier'].'"';
        $q .= ')';
        return $q;
    }

    /**
     * $iaa =  [
     *     'id_category' => (int)id,
     *     'id_parent' => (int)id,
     *     'icecat' => string(1024),
     *     'supplier' => string(1024),
     *     'category' => string(1024),
     * ];
     */
    function add(array $iaa) {
        if (!array_key_exists('supplier', $iaa)) {
            die('supplier_name is not set in array.');
        }
        if (!array_key_exists('category', $iaa)) {
            die('category_name is not set in array.');
        }

        if (!$this->checkExiste($iaa)) {
            $q = $this->add_q($iaa);
            if ($this->exec_q($q))
                return true;
            echo 'Donnée de liaison incorrecte.';
            die('add::false 0');
        }
        return false;
    }
}

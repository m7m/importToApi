<?php

/**
 * Csv to Prestashop
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */

namespace FileCache;

use DateTime;

class FileCache
{
    function __construct(
        $absolute_path = '/tmp/FileCache',
        $time_cache_sec = 99259200
    )
    {
        $this->tmp = $absolute_path;
        $this->time_cache_sec = $time_cache_sec;
        $cr_dir = 0;
        if (!is_dir($absolute_path))
            $cr_dir = 1;
        // else if (!$this->checkTimeFileCache($absolute_path))
        //     $cr_dir = 1;
        if ($cr_dir)
            mkdir($absolute_path, 0777, true);
    }

    function checkTimeFileCache($filename)
    {
        $date_file = filemtime($filename);
        $date_file_max = (new DateTime())->getTimestamp() - $this->time_cache_sec;
        // pdebug('checkTimeFileCache::$date_file',$date_file);
        // pdebug('checkTimeFileCache::$date_file_max',$date_file_max);
        return ($date_file >= $date_file_max);
    }

    function get($uri_pathfile, $verbose = true)
    {
        if ($uri_pathfile !== '/' && !strncmp($uri_pathfile, '/', 1))
            return $uri_pathfile;

        $filepath = parse_url($uri_pathfile, PHP_URL_PATH);
        if (!$filepath && $filepath === '/')
            $filepath = 'racine';
        $filecache = $this->tmp . '/' . $filepath;

        // pdebug('FileCache::file_exists',file_exists($filecache));
        // pdebug('FileCache::checkTimeFileCache',$this->checkTimeFileCache($filecache));

        if (file_exists($filecache)
            && $this->checkTimeFileCache($filecache)) {
            if ($verbose)
                $v = 'C';
        } else {
            if ($verbose)
                echo 'D';
            if (!$filetmp = file_get_contents($uri_pathfile)) {
                echo 'Error file access: ' . $uri_pathfile .PHP_EOL;
                return false;
            }
            $pathname = dirname($filecache);
            if (!file_exists($pathname)
                && !mkdir($pathname, 0777, true)) {
                echo 'Error file access: ' . $pathname.PHP_EOL;
                return false;
            }
            if (!file_put_contents($filecache, $filetmp)) {
                echo 'Error write file: ' . $filecache.PHP_EOL;
                return false;
            }
        }
        return $filecache;
    }
}

?>

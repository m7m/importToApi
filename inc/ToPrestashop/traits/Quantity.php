<?php

namespace ToPrestashop;

trait Quantity
{
    function getQuantity($dispo)
    {
      //  pdebug('getQuantity::dispo', $dispo);
        $q = -1;
        if (self::$visible === 0)
            return 0;
        if (is_numeric($dispo) // && is_int($dispo)
        )
            $q = intval($dispo);
        else if ($dispo === 'nd')
            $q = 0;
        else if ($dispo === 'l')
            $q = 3;
        else if ($dispo === 'd')
            $q = 6;
        return $q;
    }
}

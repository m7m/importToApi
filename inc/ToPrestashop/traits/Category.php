<?php

/**
 * Csv to Prestashop
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */

namespace ToPrestashop;

use DOMXpath;

trait Category
{
    function setRelFour(array $param_rfi) {
        if (!empty($param_rfi) && $this->rfi) {
            /* $this->genName($param_rfi['icecat'], 255); */
            // ajout liaison icecat<->fournisseur
            $this->rfi->add($param_rfi);
                // echo "setCategory:: "
                //     .$param_rfi['icecat_name']
                //     .':'.$param_rfi['supplier_name']
                //     .':'.$param_rfi['category_name']
                //     .':';
            // die('setCategory::param_rfi::not implemented');
        }
    }        

    function setCategory($cat_name, $id_parent, $images) {
        $xml = self::$xml_categories;
        $xml_c = &$xml->category;

        {
            unset ($xml_c->level_depth);
            unset ($xml_c->nb_products_recursive);
            unset ($xml_c->description);
            unset ($xml_c->meta_title);
            unset ($xml_c->meta_description);
            unset ($xml_c->meta_keywords);
            unset ($xml_c->associations);
            unset ($xml_c->position);
            unset ($xml_c->date_add);
            unset ($xml_c->date_upd);
            unset ($xml_c->is_root_category);
            unset ($xml_c->id);
        }

        $xml_c->id_parent = $id_parent;
        $xml_c->active = 1;
        $xml_c->id_shop_default = self::$id_shop;

        unset ($xml_c->name->language);
        unset ($xml_c->link_rewrite->language);
        $this->genLinkRewriteAndName($cat_name, $xml_c, 1);

        // pdebug('setCategory::$cat_name',$cat_name);
        // pdebug('setCategory::$cat_name_icecat',$cat_name_icecat);
        $opt = $this->getOptPost('categories', $xml);
        $data_xml = $this->psw->add($opt);
        $id = (int)$data_xml->category->id;

        if (!empty($images) && is_array($images))
            $images = $images[0];
        if (self::$enable_image) {
            $this->addImages($images, $id, 'categories');
            echo ' ';
        }
        return $id;
    }

    // permet de forcer la mise a jour des categories
    function setCategoryUpdate($xml_get, $images, array $param_rfi) {
        $xml = self::$xml_categories;
        $xml_c = &$xml->category;
        $xml_get_c = &$xml_get->categories->category;
        $id_cat = (int)$xml_get->categories->category->id;
        
        {
            unset ($xml_c->level_depth);
            unset ($xml_c->nb_products_recursive);
            $xml_c->description = $xml_get_c->description;
            $xml_c->meta_title = $xml_get_c->meta_title;
            $xml_c->meta_description = $xml_get_c->meta_description;
            $xml_c->meta_keywords = $xml_get_c->meta_keywords;
            $xml_c->associations = $xml_get_c->associations;
            $xml_c->position = $xml_get_c->position;
            // $xml_c->date_add = $xml_get_c->date_add;
            unset($xml_c->date_add);
            $xml_c->date_upd = $xml_get_c->date_upd;
            $xml_c->is_root_category = $xml_get_c->is_root_category;
            $xml_c->id_parent = $xml_get_c->id_parent;
            $xml_c->id_shop_default = $xml_get_c->id_shop_default;
            $xml_c->id = $id_cat;
            // $xml_c->active = $xml_get_c->active;
            $xml_c->active = 1;
        }

        unset ($xml_c->name->language);
        unset ($xml_c->link_rewrite->language);
        // $this->genLinkRewriteAndName($cat_name_icecat, $xml_c, self::$id_lang_icecat_category);

        // insert rfi
        // pdebug('cat update::$param_rfi',$param_rfi);
        // pdebug('cat update::$param_rfi nn',$param_rfi['icecat_name']);
        $this->genName($param_rfi['icecat'], 255);
        // ajout liaison icecat<->fournisseur
        $this->rfi->add($param_rfi);
        return ;
        die('revoir la ligne ci dessous');

        // pdebug('$xml::',$xml);
        $opt = $this->getOptPut('categories', $xml, $id_cat);
        //pdebug('$opt::',$opt);
        $data_xml = $this->psw->edit($opt);
        // pdebug('$data_xml::',$data_xml);
        $id = (int)$data_xml->category->id;
        return $id;
    }

    function getIdCategory(
        $cat_name, $images,
        array $param_rfi = []
    )
    {
        $cat_name = ucfirst(trim($cat_name));
        $cat_name = $this->genName($cat_name);
        $opt = [
            'resource' => 'categories',
            'display'=>'[id]',
            'filter[name]'=> '['.$cat_name.']',
        ];
        $data_xml = $this->psw->get($opt);
        if (!($id = $data_xml->categories->category->id)) {
            $id = $this->setCategory(
                $cat_name,
                self::$default_disable_category_id, $images
            );
        }
        return $id;
    }

    function getIdCategoryToRel(array $param_rfi) {
        if ($this->rfi) {
            $r = $this->rfi->getRelIdCategory($param_rfi);
            if (!$r)
                $r = 0;
        }
        return $r;
    }

    // (split / )
    function getIdCategoryDefault(
        $cat_name, $images,
        array $param_rfi = []
    )
    {
        $cat_lst = explode('/', $cat_name);
        $id_category_accueil = 2;
        $id_parent = $id_category_accueil;
        $opt = [
            'resource' => 'categories',
            'display'=>'full',
        ];
        $i = 0;
        $nb_cat = count($cat_lst) - 2;// x0->x1=2->x2
        $param_rfi['category'] = null;
        foreach ($cat_lst as $cat_name) {
            $cat_name = ucfirst(trim($cat_name));
            $cat_name = $this->genName($cat_name);
            if (empty($cat_name)) {
                continue ;
            }
            $opt['filter[name]'] = '['.$cat_name.']';
            $opt['filter[id_parent]'] = '['.$id_parent.']';
            $data_xml = $this->psw->get($opt);
            if (!($id = $data_xml->categories->category->id)) {
                $id = $this->setCategory($cat_name, $id_parent, $images);
            }

            if ($param_rfi['category'])
                $param_rfi['category'] .= '/'.$cat_name;
            else
                $param_rfi['category'] = $cat_name;
            $param_rfi['id_category'] = $id;
            $param_rfi['id_parent'] = $id_parent;

            $this->setRelFour($param_rfi);
            $i += 1;
            $id_parent = $id;
        }
        return (int)$id;
    } 
        
}

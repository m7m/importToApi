<?php

/**
 * Csv to Prestashop
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */

namespace ToPrestashop;

use DOMXpath;

trait Manufacturer
{
    /**
     * @param string $brand nom de la marque
     *
     * @return int|false
     */
    function getIdManufacturer($brand)
    {
        $brand = trim($brand);
        if (empty($brand))
            return 0;
        if (!($id = $this->getManufacturer($brand)))
            $id = $this->manufacturerAdd($brand);
        return $id;
    }

    function getManufacturer($brand)
    {
        $opt = $this->getOptGetByName('manufacturers', $brand);
        //pdebug('getManufacturer::$opt',$opt);
        $data_xml = $this->psw->get($opt);
        $id = 0;
        if ($data_xml->manufacturers)
            if ($data_xml->manufacturers->manufacturer) {
                $id = (int)$data_xml->manufacturers->manufacturer->id;
            }
        return $id;
    }

    function manufacturerOptPost($data_xml)
    {
        return $this->getOptPost('manufacturers', $data_xml);
    }

    function manufacturerAdd($brand)
    {
        $xml = self::$xml_manufacturers;
        $xml->manufacturer->name = $brand;
        $xml->manufacturer->active = 1;
        $opt = $this->manufacturerOptPost($xml);
        //pdebug('manufacturerAdd::$opt',$opt);
        $ret = $this->psw->add($opt);
        //pdebug('manufacturerAdd::$ret',$ret);
        return (int)$ret->manufacturer->id;
    }
}

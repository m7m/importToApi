<?php

/**
 * Csv to Prestashop
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */

namespace ToPrestashop;

trait ProductFeatures
{

    function getIdFeature($feature_name)
    {
        $opt = $this->getOptGetByName('product_features', $feature_name);
        $data_xml = $this->psw->get($opt);
        //pdebug('getIdFeature::$data_xml',$data_xml);

        $id = false;
        if ($data_xml->product_features)
            if ($data_xml->product_features->product_feature) {
                $id = (int)$data_xml->product_features->product_feature->id;
            }
        //pdebug('getIdFeature::$id',$id);
        return $id;
    }

    public static $replace_in_feature = [
        '='
    ];
    public static $replace_ou_feature = [
        '-'
    ];
    function postFeature($feature_name, $feature_position = false)
    {
        $xml_feature = self::$xml_product_features;
        $xml = &$xml_feature->product_feature;
        
        $nb_lang = count($xml->name->language);
        $i = 0;

        $feature_name = str_replace(
            self::$replace_in_feature, self::$replace_ou_feature,
            $feature_name
        );
        
        while ($i < $nb_lang) {
            $xml->name->language[$i] = $feature_name;
            $i += 1;
        }
        if ($feature_position) {
            $xml->position = $feature_position;
        } else
            unset ($xml->position);

        //pdebug('postFeature::$xml_feature',$xml_feature);
        $opt = $this->getOptPost('product_features', $xml_feature);
        $ret = $this->psw->add($opt);

        //pdebug('postFeature::$ret',$ret);
        return (int)$ret->product_feature->id;
        //return (int)$ret->product_features->product_feature->id;
    }

    function getIdFeatureValue($feature_value, $id_feature)
    {

        $opt = [
            'resource' => 'product_feature_values',
            'display' => '[id]',
            'filter[id_feature]'=> $id_feature,
            'filter[value]'=> '['.$feature_value.']'
        ];
        // $opt = $this->getOptGetByName('product_feature_values', $feature_value);

        //pdebug('getIdFeatureValue::$opt',$opt);
        $data_xml = $this->psw->get($opt);
        //pdebug('getIdFeatureValue::$data_xml',$data_xml);
        //die('test');
        
        $id = false;
        if ($data_xml->product_feature_values)
            if ($data_xml->product_feature_values->product_feature_value) {
                $id = (int)$data_xml
                    ->product_feature_values
                    ->product_feature_value
                    ->id;
            }
        //pdebug('getIdFeatureValue::$id',$id);
        return $id;
    }

    function postFeatureValue($feature_value, $id_feature)
    {
    
        $xml_feature = self::$xml_product_feature_values;
        $xml = &$xml_feature->product_feature_value;

        $xml->id_feature = (int)$id_feature;
        unset($xml->id);
        unset($xml->custom);
        $nb_lang = count($xml->value->language);
        $i = 0;
        while ($i < $nb_lang) {
            $xml->value->language[$i] = $feature_value;
            $i += 1;
        }

        //pdebug('postFeatureValue::$xml_feature',$xml_feature);
        //die('test');
        
        $opt = $this->getOptPost('product_feature_values', $xml_feature);
        $ret = $this->psw->add($opt);

        //pdebug('postFeatureValue::$ret',$ret);
        return (int)$ret->product_feature_value->id;
    }

    public static $replace_in_feature_value = [
        '<br>','<sup>','</sup>','/','<','='
    ];
    public static $replace_ou_feature_value = [
        ', '  ,''     ,''      ,',','' ,':'
    ];

    public static $replace_in_feaname = [
        ' < ', "\xC2\xA0"
    ];
    public static $replace_ou_feaname = [
        ' inférieur à ', ' '
    ];

    function setXmlFeatures(&$xml_features, array $features)
    {
        $i = 0;
        //pdebug('setXmlFeatures::$xml_features->product_feature', $xml_features->product_feature);
        foreach ($features as $familly_name)
        {
            foreach ($familly_name as $feature_name => $feature_value)
            {
                //pdebug('$feature_name::$feature_value',$feature_name.'::'.$feature_value);
                
                //pdebug('setXmlFeatures::$feature_name',$feature_name);
                //pdebug('setXmlFeatures::$feature_value',$feature_value);
                $feature_name_t = trim($feature_name);
                $feature_name_t = str_replace(
                    self::$replace_in_feaname,
                    self::$replace_ou_feaname,
                    $feature_name_t
                );
                
                if (!$id_feature = $this->getIdFeature($feature_name_t))
                    $id_feature = $this->postFeature($feature_name_t, $i);

                $feature_value_t = trim($feature_value);
                $this->maxLine($feature_value_t, 254);
                //pdebug('setXmlFeatures::$name', $feature_value_t);

                $feature_value_t = str_replace(
                    self::$replace_in_feature_value,
                    self::$replace_ou_feature_value,
                    $feature_value_t
                );
                

                if (!strncmp($feature_value_t, 'http://', 7))
                    continue ;

                if (!$id_feature_value = $this->getIdFeatureValue(
                    $feature_value_t, $id_feature
                ))
                    $id_feature_value = $this->postFeatureValue(
                        $feature_value_t, $id_feature
                    );
                
                // //pdebug('setXmlFeatures::$xml_features',$xml_features);
                // pdebug('setXmlFeatures::$id_feature',$id_feature);
                // pdebug('setXmlFeatures::$id_feature_value',$id_feature_value);
                $xml_features
                    ->product_feature[$i]
                    ->id = $id_feature;
                $xml_features
                    ->product_feature[$i]
                    ->id_feature_value = $id_feature_value;
                    //->addChild('id_feature_value', $id_feature_value;
                $i += 1;
            }
            echo '.';
        }
        //pdebug('setXmlFeatures::$xml_features->product_feature 2', $xml_features->product_feature);
        //pdebug('setXmlFeatures::$xml_features',$xml_features);
        return $i;
    }
}

<?php

/**
 * Csv to Prestashop
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */

namespace ToPrestashop;

use DOMXpath;

trait Supplier
{
    /**
     * @param string $supplier nom du fournisseur
     *
     * @return int|false
     */
    function getIdSupplier($supplier)
    {
        $supplier = trim($supplier);
        if (empty($supplier))
            return 0;
        if (!($id = $this->getSupplier($supplier)))
            $id = $this->supplierAdd($supplier);
        $this->id_supplier = $id;
        return $id;
    }

    function getSupplier($supplier)
    {
        $opt = $this->getOptGetByName('suppliers', $supplier);
        //pdebug('getSupplier::$opt',$opt);
        $data_xml = $this->psw->get($opt);
        $id = 0;
        if ($data_xml->suppliers)
            if ($data_xml->suppliers->supplier) {
                $id = (int)$data_xml->suppliers->supplier->id;
            }
        return $id;
    }

    function supplierOptPost($data_xml)
    {
        return $this->getOptPost('suppliers', $data_xml);
    }

    function supplierAdd($supplier)
    {
        $xml = self::$xml_suppliers;
        $xml->supplier->name = $supplier;
        $xml->supplier->active = 1;
        $opt = $this->supplierOptPost($xml);
        //pdebug('supplierAdd::$opt',$opt);
        $ret = $this->psw->add($opt);
        //pdebug('supplierAdd::$ret',$ret);
        return (int)$ret->supplier->id;
    }
}

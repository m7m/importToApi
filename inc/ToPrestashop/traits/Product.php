<?php

/**
 * Csv to Prestashop
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */

namespace ToPrestashop;

require 'Category.php';
require 'ProductFeatures.php';
require 'Product_suppliers.php';
require 'StockAvailables.php';

use DOMXpath;

trait Product
{
    use Category;
    use ProductFeatures;
    use Product_supplier;
    use StockAvailables;
    
    /**
     * @param string $ref référence constructeur du produit
     *
     * @return int|false
     */
    function getIdProduct($ref = null)
    {
        if ($this->id > 0)
            return $this->id;
        if (empty($ref))
            return false;

        $ref = trim($ref);
        $opt = [
            'resource' => 'products',
            'display'=>'[id,active,price,id_supplier]',
            'filter[reference]'=> '['.$ref.']',
        ];
        //pdebug('getIdProduct::$opt',$opt);
        $data_xml = $this->psw->get($opt);
        // pdebug('getIdProduct::$data_xml',$data_xml);

        $this->id = false;
        if ($data_xml->products)
            if ($data_xml->products->product) {
                $this->id = (int)$data_xml->products->product->id;
                $this->cur_price = (int)$data_xml->products->product->price;
                $this->cur_active = (int)$data_xml->products->product->active;
                $this->cur_id_supplier = (int)$data_xml->products->product->id_supplier;
                // pdebug('cur_id_supplier',$this->cur_id_supplier);
            }
        //pdebug('getIdProduct::$this->id',$this->id);
        return $this->id;
    }

    /**
     * @param string $ref référence fournisseur du produit
     *
     * @return int|false
     */
    function getIdProductSupplier($ref = null)
    {
        if ($this->id > 0)
            return $this->id;
        if (empty($ref))
            return false;

        $ref = trim($ref);
        $opt = [
            'resource' => 'product_suppliers',
            'display'=>'[id_product]',
            //'filter[supplier_reference]'=> '['.$ref.']',
            'filter[product_supplier_reference]'=> '['.$ref.']',
        ];
        // pdebug('getIdProduct::$opt',$opt);
        $data_xml = $this->psw->get($opt);
        // pdebug('getIdProduct::$data_xml',$data_xml);

        $this->id = false;
        if ($data_xml->product_suppliers)
            if ($data_xml->product_suppliers->product_supplier) {
                $this->id = (int)$data_xml->product_suppliers->product_supplier->id_product;
            }
        //pdebug('getIdProduct::$this->id',$this->id);
        return $this->id;
    }

    /**
       [id]
       [id_manufacturer]
       [id_supplier]
       [id_category_default]
       [new]
       [cache_default_attribute]
       [id_default_image]
       [id_default_combination]
       [id_tax_rules_group]
       [position_in_category]
       [type]
       [id_shop_default]
       [reference]
       [supplier_reference]
       [location]
       [width]
       [height]
       [depth]
       [weight]
       [quantity_discount]
       [ean13]
       [isbn]
       [upc]
       [cache_is_pack]
       [cache_has_attachments]
       [is_virtual]
       [state]
       [on_sale]
       [online_only]
       [ecotax]
       [minimal_quantity]
       [price]
       [wholesale_price]
       [unity]
       [unit_price_ratio]
       [additional_shipping_cost]
       [customizable]
       [text_fields]
       [uploadable_files]
       [active]
       [redirect_type]
       [id_type_redirected]
       [available_for_order]
       [available_date]
       [show_condition]
       [condition]
       [show_price]
       [indexed]
       [visibility]
       [advanced_stock_management]
       [date_add]
       [date_upd]
       [pack_stock_type]
       [meta_description]
       [meta_keywords]
       [meta_title]
       [link_rewrite]
       [name]
       [description]
       [description_short]
       [available_now]
       [available_later]
       [associations]
    */

    function replace_WDH($str)
    {
        return preg_replace(
            [
                '/, [0-9]+/',
                '/,/',
                '/ in/',
                '/ mm/',
                '/ cm/'
            ],
            [
                '',
                '.',
                ''
            ],
            $str
        );
    }
    
    /**
     * Gestion des dimenssions
     */
    function prodDataWDH(array $prod, &$xml)
    {
        if (array_key_exists(INDEX_CARACTERISTIQUE, $prod)
            && !empty($prod[INDEX_CARACTERISTIQUE])) {
            $features = $prod[INDEX_CARACTERISTIQUE];
            if ($prod[INDEX_CARACTERISTIQUE_LANG] === 'FR') {
                $lang_ped = 'Poids et dimensions';
                $lang_width = 'Largeur';
                $lang_depth = 'Profondeur';
                $lang_height = 'Hauteur';
            } else if ($prod[INDEX_CARACTERISTIQUE_LANG] === 'EN') {
                $lang_ped = 'Weight & dimensions';
                $lang_width = 'Width';
                $lang_depth = 'Depth';
                $lang_height = 'Height';
            }

            if (array_key_exists($lang_ped, $features)
            ) {
                $ped = $features[$lang_ped];
                if (array_key_exists($lang_width, $ped)) {
                    $xml->product->width = $this->replace_WDH($ped[$lang_width]);
                }
                if (array_key_exists($lang_depth, $ped)) {
                    $xml->product->depth = $this->replace_WDH($ped[$lang_depth]);
                }
                if (array_key_exists($lang_height, $ped)) {
                    $xml->product->height = $this->replace_WDH($ped[$lang_height]);
                }
            }
        }
    }

    // $xml_feature = provenant de:
    //  product->associations->product_features->product_feature
    // $features = [familly => [name_feature => feature_value ], ...]
    function prodDataFeatures(&$xml_features, $features, $features_lang)
    {
        echo 'feature:';
        if (!empty($features) && is_array($features)) {
            //pdebug('prodDataFeatures::$features',$features);
            $nb = $this->setXmlFeatures($xml_features, $features);
            echo $nb.':';
        } else
            echo '0:';

    }

    /**
     * Relation de la catégoie
     * Si fournisseur principal nous ignorons icecat
     * Sinon cherche icecat
     *  si relation trouvé nous injectons normalement
     *  sinon nous injecton dans la catégories non trié
     */
    function prodDataCategories($prod, $id_supplier) {
        $id_cat = 0;
        $cat_name = $prod[INDEX_CATEGORIES];
        $param_rfi = [
            'id_category' => 0,
            'id_parent' => 0,
            'status' => 0,
            'icecat' => NULL,
            'supplier' => mb_strtoupper($prod[INDEX_FOURNISSEUR]),
            'category' => $this->genName($cat_name, 1024)
        ];
        if (array_key_exists(INDEX_CATEGORIES_ICECAT, $prod)) {
            $param_rfi['icecat'] = $prod[INDEX_CATEGORIES_ICECAT];
        }

        // si fournisseur est le fournisseur principal alors les catégories se base sur lui
        if (self::$create_category
            && !(strcasecmp($prod[INDEX_FOURNISSEUR],self::$defaut_fournisseur_name))) {
            $id_cat = $this->getIdCategoryDefault(
                $prod[INDEX_CATEGORIES],
                $prod[INDEX_IMAGES],
                $param_rfi
            );
        } else {
            $id_cat = $this->getIdCategoryToRel($param_rfi);

            if (!$id_cat) {
                $cat_name = mb_strcut($prod[INDEX_FOURNISSEUR], 0,2).'-'.$prod[INDEX_CATEGORIES];
                if (strncasecmp($cat_name, 'AL-', 3)
                && strncasecmp($cat_name, 'EC-', 3)
                && strncasecmp($cat_name, 'AC-', 3))
                    die('Error: Valeur fournisseur incongru:'.$cat_name.':'.$prod[INDEX_FOURNISSEUR]);
                $id_cat = $this->getIdCategory($cat_name, $prod[INDEX_IMAGES], $param_rfi);
            }
        }

        if (!$id_cat || empty($id_cat))
            die('prodDataCategories::implementation en erreur');
        return $id_cat;
    }
    
    function prodData(array $prod)
    {
        $xml = self::$xml_product;
        $xml_p = &$xml->product;
        $xml_p->id_manufacturer = $this->getIdManufacturer($prod[INDEX_MARQUE]);

        $xml_p->id_tax_rules_group = self::$default_tax_rules_group_id;
        $xml_p->id_shop_default = self::$id_shop;

        $xml_p->supplier_reference = $prod[INDEX_REF_FOURNISSEUR];
        $ref = $prod[INDEX_REF_CONSTRUCTEUR];
        $this->maxLine($ref, 32);
        if (empty($ref))
            $ref = $xml_p->supplier_reference;
        $xml_p->reference = $ref;

        $this->prodDataWDH($prod, $xml);
        $xml_p->weight = $prod[INDEX_POID];

        if (!is_numeric($prod[INDEX_EAN_CODE]))
            $ean_upc = '';
        else
            $ean_upc = $prod[INDEX_EAN_CODE];
        $len_ean_upc = strlen($ean_upc);
        if ($len_ean_upc < 14)
            $xml_p->ean13 = $ean_upc;

        // $xml_p->on_sale = $this->getOnSale();
        // $xml_p->online_only = $this->getOnlineOnly();
        //$xml_p->wholesale_price = 123;// prix d'achat chez le fournisseur

        $xml_p->new = self::$new;
        //$xml_p->active = 1;
        //$xml_p->visibility = 'both';
        $xml_p->state = 1;// active produit backOffice
        $xml_p->show_price = 1;
        $xml_p->available_for_order = 1;//disponible pour la vente
        $xml_p->redirect_type = '404';
        $xml_p->minimal_quantity = 1;

        
        unset ($xml_p->name->language);
        $this->genLinkRewriteAndName($prod[INDEX_LIBELLE_PRODUIT], $xml_p, 1);
        
        $xml_p->description = str_replace('\\"', '"', $prod[INDEX_DESCRIPTIONS_LONG]);
        $xml_p->description_short = $prod[INDEX_DESCRIPTIONS_COURT];

        //pdebug('prodData::$xml',$xml);
        return $xml;
    }

    function prodFeatures($prod, &$xml)
    {
        if (self::$enable_feature) {
            if (!array_key_exists(INDEX_CARACTERISTIQUE_LANG, $prod))
                $prod[INDEX_CARACTERISTIQUE_LANG] = 'FR';
            $this->prodDataFeatures(
                $xml->product->associations->product_features,
                $prod[INDEX_CARACTERISTIQUE],
                $prod[INDEX_CARACTERISTIQUE_LANG]
            );
        }
    }
    
    function prodAdd(array $prod)
    {
        echo 'add:';
        self::$new = 1;
        $xml = $this->prodData($prod);
        $xml_p = &$xml->product;
        $id_supplier = $this->getIdSupplier($prod[INDEX_FOURNISSEUR]);
        $xml_p->id_supplier = $id_supplier;

        $id_cat = $this->prodDataCategories($prod, $id_supplier);
        if (!$id_cat)
            return false;
        $xml_p->id_category_default = $id_cat;
        $xml_p->associations->categories->category->id = $id_cat;

        $xml_p->ecotax = $prod[INDEX_ECOTAX];
        $xml_p->price = $prod[INDEX_PRIX_HTT];
        unset($xml->product->id);

        $this->prodFeatures($prod, $xml);

        $xml->product->active = $this->active; // en ligne
        $opt = $this->prodOptPost($xml);
        $ret = $this->psw->add($opt);
        $this->id = $ret->product->id;
        $this->prodSupplierAdd($prod[INDEX_REF_FOURNISSEUR], $prod['SUPPLIER_PRIX_HTT']);
        return $ret;
    }

    function prodUpdate(array $prod)
    {
        //pdebug('prodUpdate');
        echo 'upd:';
        self::$new = 0;
        $xml = $this->prodData($prod);
        $xml_p = &$xml->product;
        $xml_p->id = $this->id;
        $xml_p->ecotax = $prod[INDEX_ECOTAX];

        // Quel fournisseur prime
        $cur_stock = $this->getStock($this->id);
        $id_supplier = $this->getIdSupplier($prod[INDEX_FOURNISSEUR]);
        if ($cur_stock == 0 || $this->cur_active == 0) {
            // pdebug('le nouveau prime');
        }

        else if ($this->active == 0
        || ($this->getQuantity($prod[INDEX_DISPONIBILITE]) == 0
        && $this->cur_id_supplier != $id_supplier)) {
            // pdebug("l'ancien prime");
            $this->active = $this->cur_active;
            $id_supplier = $this->cur_id_supplier;
            $prod[INDEX_DISPONIBILITE] = $this->getStock($this->id);
            $prod[INDEX_PRIX_HTT] = $this->cur_price;
        }

        $xml_p->id_supplier = $id_supplier;
        $xml_p->price = $prod[INDEX_PRIX_HTT];

        $id_cat = $this->prodDataCategories($prod, $id_supplier);
        if (!$id_cat) {
            return false;
        }
        $xml_p->id_category_default = $id_cat;
        $xml_p->associations->categories->category->id = $id_cat;
        
        //$xml->product->new = 1;
        $xml->product->active = $this->active; // en ligne
        //$xml->product->visibility = 1;
        // $xml->product->state = 1;// active produit backOffice

        $this->putSupplierAdd($prod[INDEX_REF_FOURNISSEUR], $prod['SUPPLIER_PRIX_HTT']);

        $this->prodFeatures($prod, $xml);
        $opt = $this->prodOptPut($xml, $this->id);
        //pdebug('$opt',$opt);
        $ret = $this->psw->edit($opt);
        return $ret;
    }
    
    public static $replace_in_ref = [
        '=','>'
    ];
    public static $replace_ou_ref = [
        '-',''
    ];
    function fixRef(&$ref)
    {
        $ref = trim($ref);
        $ref = str_replace(
            self::$replace_in_ref, self::$replace_ou_ref, $ref
        );
    }
    
    function sendProd(array $prod)
    {
        $this->fixRef($prod[INDEX_REF_CONSTRUCTEUR]);
        $this->fixRef($prod[INDEX_REF_FOURNISSEUR]);

        $r = $this->checkExisteProd($prod);
        if ($r === -1) {
            $ret = false;
        }
        else if ($r)
            $ret = $this->prodUpdate($prod);
        else
            $ret = $this->prodAdd($prod);

        if (!$ret) {
            echo 'ko';
            return false;
        }
        echo 'ok';
        $this->id_stock_availables = (int)$ret
                                   ->product
                                   ->associations
                                   ->stock_availables
                                   ->stock_available
                                   ->id;
        $this->addProductImages($prod[INDEX_IMAGES]);
        $this->updateStock($prod[INDEX_DISPONIBILITE]);
        return true;
    }

    function addProductImages($images)
    {
        if (self::$enable_image)
            return $this->addImages($images, $this->id, 'products');
        return false;
    }

    function prodOptPost($data_xml)
    {
        return $this->getOptPost('products', $data_xml);
    }
    function prodOptPut($data_xml, $id)
    {
        return $this->getOptPut('products', $data_xml, $id);
    }
}
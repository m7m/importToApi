<?php

/**
 * Csv to Prestashop
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */

namespace ToPrestashop;

use CurlFile;
use SimpleXML;
use SimpleXMLElement;

trait ProductImage
{
    function imageErr($data, $status, $file)
    {

        if ($data) {
            $xml = new SimpleXMLElement($data);
            if ($xml->errors->error) {
                printf(
                    'Error send image, message:'.$xml->errors->error->message
                    .PHP_EOL
                    .'code prestashop:'.$xml->errors->error->code
                    .PHP_EOL
                    .'code:'.$status.PHP_EOL
                    .'file:'.$file.PHP_EOL
                );
                
            }
        } else
            printf('Error send image, code:'.$status.PHP_EOL
                   .'file:'.$file.PHP_EOL
            );
    }

    function checkImageMime(&$file)
    {
        if (mime_content_type($file) === 'image/tiff') {
            $image = new \Imagick();           //création d'un objet Imagick
            $image->pingImage($file); //ping de l'image
            $image->readImage($file); //on enregistre l'image dans l'objet
            $image->setImageFormat('jpg');    //on change de format (apriori on peu s'en passer avec la ligne suivante
            $image->writeImage($file . '.jpg');   //on crée un fichier image
            $file .= '.jpg';
        }
    }
    
    function postImage($file, $id, $resource)
    {
        // cache
        $file = $this->fc->get($file);
        if ($file)
            $this->checkImageMime($file);

        $ch = curl_init();
        $uri = self::$apiUri.'/api/images/'.$resource.'/'.$id;
        // pdebug('postImage::$uri',$uri);
        // pdebug('addImages::$file', $file);
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERPWD, self::$apiToken.':');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt(
            $ch, CURLOPT_POSTFIELDS,
            ['image' => new CurlFile($file)]
        );

        $data = curl_exec ($ch);
        $status = curl_getinfo($ch)['http_code'];
        curl_close($ch);
        //pdebug('postImage::$data',$data);
        //pdebug('postImage::$status',$status);
        if ($status != 200) {
            $this->imageErr($data, $status, $file);
            return false;
        }
        return true;
    }

    function checkImageCategoires($id, $resource)
    {

        $ch = curl_init();
        $uri = self::$apiUri.'/api/images/'.$resource.'/'.$id;
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_USERPWD, self::$apiToken.':');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $data = curl_exec ($ch);
        $status = curl_getinfo($ch)['http_code'];
        curl_close($ch);
        // pdebug('checkImageCategoires::$data',$data);
        // pdebug('checkImageCategoires::$status',$status);
        if ($status != 200) {
            $q = false;
        } else
            $q = true;

        $hasimage = 0;
        if ($q && $data && $data->prestashop) {
            if ($data->prestashop->errors->error) {
                if ($data->prestashop->errors->error->code != 61
                && $data->prestashop->errors->error
                    ->message != 'This image does not exist on disk') {
                    $hasimage = 1;
                }
            }
        }
        return $hasimage;
    }

    /**
     * @return array
     **/
    function getIdImageProduct($id)
    {
        $id_arr = [0=>0];
        $id_img = 0;
        $this->checkImage($id, 'products', $id_arr);
        pdebug('id img::',$id_arr);
        return $id_arr;
    }
    
    function checkImage($id, $resource, &$id_img = NULL)
    {
        if (!$id)
            return -1;

        $hasimage = 0;
        if ('products' === $resource) {
            $opt = [
                'resource' => $resource,
                'display'=>'full',
                'filter[id]'=>'['.$id.']'
            ];
            $data_xml = $this->psw->get($opt);

            if ($data_xml->products->product) {
                $dx_ppai = $data_xml->products->product->associations->images;
                if ($dx_ppai)
                    if ($dx_ppai->image) {
                        $hasimage = count($dx_ppai->image);
                        if ($id_img) {
                            $i = 0;
                            foreach ($dx_ppai as $img) {
                                $id_img[$i] = (int)$dx_ppai->image->id;
                                $i += 1;
                            }
                            /* pdebug('id img::',$id_img); */
                        }
                    }
            }

        } else if ('categories' === $resource) {
            $this->checkImageCategoires($id, $resource);
         }
        return $hasimage;
    }

    function delImages($id, array $id_img_lst)
    {
        foreach ($id_img_lst as $id_img) {
            echo 'clear:';
            $opt = [
                'url' => API_URI .'/api/images/products/'.$id.'/'.$id_img
            ];
            $o->delete($opt);
        }
    }
    
    function addImages($images, $id, $resource)
    {
        //pdebug('addImages::$images', $images);
        echo ' Image/'.$resource.':';
        if (!is_array($images))
            $images = [$images];
        if (empty($images))
            $nb_new = 0;
        else
            $nb_new = count($images);
        $id_img_lst = [];
        $nb_old = $this->checkImage($id, $resource, $id_img_lst);
        $i = 0;
        if ($nb_new) {

            if (!self::$new && $nb_old === 0 && $nb_new > 1) {
                $this->delImages($id, $id_img_lst);
                $nb_old = $this->checkImage($id, $resource);
            }
            
            if ($nb_old === 0)
            {
                foreach ($images as $name_img => $image) {
                    if (!empty($image))
                        if ($this->postImage($image, $id, $resource))
                            $i += 1;
                }
            } else
                $i = $nb_old;
        } else
            $i = $nb_old;
        echo ':'.$i.'/'.$nb_new;
    }
}
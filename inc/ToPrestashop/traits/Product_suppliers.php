<?php

/**
 * Csv to Prestashop
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */

namespace ToPrestashop;

use DOMXpath;

trait Product_supplier
{
    function prodSupplierAdd($ref_supplier, $price_supplier = 0)
    {
        $xml = self::$xml_product_suppliers;
        $xml->product_supplier->id_product = $this->id;
        $xml->product_supplier->id_product_attribute = 0;
        $xml->product_supplier->id_supplier = $this->id_supplier;
        $xml->product_supplier->id_currency = 1;
        $xml->product_supplier->product_supplier_reference = $ref_supplier;
        $xml->product_supplier->product_supplier_price_te = $price_supplier;
        $opt = $this->getOptPost('product_suppliers', $xml);
        /* pdebug('prodSupplierAdd::$opt',$opt); */
        $ret = $this->psw->add($opt);
        //pdebug('prodSupplierAdd::$ret',$ret);
    }

    function getCheckProductSupplier($id, $id_product)
    {
        $opt = [
            'resource' => 'product_suppliers',
            'display'=>'full',
            'filter[id_supplier]'=> '['.$id.']',
            'filter[id_product]'=> '['.$id_product.']',
        ];
        $data_xml = $this->psw->get($opt);
        /* $c = false; */
        /* pdebug('$data_xml', $data_xml); */
        if ($data_xml->product_suppliers)
            return $data_xml;
            /* if ($data_xml->product_suppliers->product_supplier) { */
            /*     $c = (int)$data_xml->product_suppliers->product_supplier->id; */
            /* } */
        return false;
    }

    function putSupplierAdd($ref_supplier, $price_supplier = 0)
    {
        if ($data_xml = $this->getCheckProductSupplier($this->id_supplier, $this->id)) {
            $id = (int)$data_xml->product_suppliers->product_supplier->id;

            $xml = self::$xml_product_suppliers;
            $xml->product_supplier->id = $id;
            $xml->product_supplier->id_product = $this->id;
            $xml->product_supplier->id_product_attribute = 0;
            $xml->product_supplier->id_supplier = $this->id_supplier;
            $xml->product_supplier->id_currency = 1;
            $xml->product_supplier->product_supplier_reference = $ref_supplier;
            $xml->product_supplier->product_supplier_price_te = $price_supplier;

            /* pdebug('$xml', $xml); */
            $opt = $this->getOptPut('product_suppliers', $xml, $id);
            /* pdebug('$opt',$opt); */
            /* pdebug('prodSupplierAdd::$opt',$opt); */
            $ret = $this->psw->edit($opt);
            //pdebug('prodSupplierAdd::$ret',$ret);
        }
	}
}

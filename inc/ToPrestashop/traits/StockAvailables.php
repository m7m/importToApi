<?php

/**
 * Csv to Prestashop
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */

namespace ToPrestashop;

require 'Quantity.php';

use DOMXpath;

trait StockAvailables
{
    use Quantity;
    
    function updateStock($dispo)
    {

        $opt = [
            'resource' => 'stock_availables',
            'id' => $this->id_stock_availables,
            'id_product_attribute' => 0
        ];
        $get_xml = $this->psw->get($opt);
        //pdebug('get::$get_xml',$get_xml);
        $quantity = $this->getQuantity($dispo);
        if (!empty($get_xml))
            if ($quantity == $get_xml->stock_available->quantity)
                return ;
            
        $xml = self::$xml_stock_availables;
        $xml->stock_available->id = $this->id_stock_availables;
        $xml->stock_available->id_shop = self::$id_shop;
        $xml->stock_available->id_product = $this->id;
        $xml->stock_available->id_product_attribute = 0;
        $xml->stock_available->quantity = $quantity;
        $xml->stock_available->minimal_quantity = 1;
        $xml->stock_available->depends_on_stock = 0;
        $xml->stock_available->out_of_stock = 2;
        
        $opt = $this->getOptPut(
            'stock_availables', $xml, $this->id_stock_availables
        );
        //pdebug('updateStock::$opt',$opt);
        $ret = $this->psw->edit($opt);
        //pdebug('updateStock::$ret',$ret);

    }

    function getStock($id_product) {
        $opt = [
            'resource' => 'stock_availables',
            'display'=>'[quantity]',
            'filter[id_product]'=> '['.$id_product.']',
        ];
        $data_xml = $this->psw->get($opt);
        if ($data_xml->stock_availables)
            if ($data_xml->stock_availables->stock_available) {
                $cur_stock = (int)$data_xml->stock_availables->stock_available->quantity;
                return $cur_stock;
            }
        die('Error stock not access');
    }
}

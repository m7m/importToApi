<?php

/**
 * Csv to Prestashop
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */

namespace ToPrestashop;

require 'PSWebServiceLibrary.php';
require 'traits/Product.php';
require 'traits/ProductImage.php';
require 'traits/Manufacturer.php';
require 'traits/Supplier.php';

// require_once 'pdebug.php';
// require_once 'oneQueryXpathCheck.php';
// require_once 'ToBase/ToBase.php';

use ToBase\ToBase;
use PSWebServiceLibrary;
use FileCache\FileCache;

/**
 * Csv to Prestashop
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <CsvToApi@m7m.fr>
 * @license  Gnu public licence version 3
 * @link     None
 */
class ToPrestashop extends ToBase
{
    use Product;
    use ProductImage;
    use Manufacturer;
    use Supplier;

    // use in CsvToApi::update()
    public static $new = 0;
    public static $visible = 1;
    // alterable in externally
    public static $id_shop = 1;
    public static $default_parent_category_id = 2;// 2 is racine
    public static $default_disable_category_id = 321;// non trié
    public static $default_tax_rules_group_id = 1;
    public static $supplier_id = 0;
    public static $tmp = 'tmp';
    public static $time_cache_sec = 2592000;
    public static $enable_feature = false;
    public static $enable_image = true;
    public static $create_category = true;

    public static $xml_product = null;
    public static $xml_categories = null;
    public static $xml_stock_availables = null;
    public static $xml_manufacturers = null;
    public static $xml_suppliers = null;
    public static $xml_product_suppliers = null;
    public static $xml_product_features = null;
    public static $xml_product_feature_values = null;

    public static $apiUri = null;
    public static $apiToken = null;
    /**
     * Utiliser pour la création des catégories
     */
    public static $defaut_fournisseur_name = 'ECP';
    public static $id_lang_icecat_category = 2;

    /**
     * Init
     *
     * @param string $apiToken Token api
     * @param string $apiKey   Key api, unused, present for conformity CsvToApi,
     *                         used for enable/disable debug
     * @param string $apiUri   Uri api, ex: http://ip.dns/prestashop
     */
    function __construct(
        $apiToken,
        $apiKey,
        $apiUri
    ) {
        self::$apiUri = $apiUri;
        self::$apiToken = $apiToken;
        $this->psw = new \PrestaShopWebservice(
            $apiUri,
            $apiToken,
            $apiKey
        );

        self::$apiUri = $apiUri;

        //$this->initSchemXml();

        // internally
        $this->id = 0;
        $this->id_supplier = 0;
        $this->active = 1;
        $this->hasimage = 0;

        // ProductImage
        $this->fc = new FileCache();

        // relFourIcecat
        $this->rfi = null;
    }

    function initSchemXml()
    {
        self::$xml_product = $this->getShemaBlank('products');
        self::$xml_categories = $this->getShemaBlank('categories');
        self::$xml_stock_availables = $this->getShemaBlank('stock_availables');
        self::$xml_manufacturers = $this->getShemaBlank('manufacturers');
        self::$xml_suppliers = $this->getShemaBlank('suppliers');
        self::$xml_product_suppliers = $this->getShemaBlank('product_suppliers');
        self::$xml_product_features = $this->getShemaBlank('product_features');
        self::$xml_product_feature_values = $this->getShemaBlank('product_feature_values');
    }
    
    /**
     * Récupere le schema xml vierge
     * @param string $resource resource
     * @return SimpleXMLElement
     */
    function getShemaBlank($resource)
    {
        //$uri = ['url' => self::$apiUri.'/api/'.$resource.'?schema=blank'];
        // return $this->psw->get($uri);

        $p = parse_url(self::$apiUri);
        $uri = self::$apiToken.'@'.$p['host'];
        if (array_key_exists('scheme', $p))
            $uri = $p['scheme'].'://'.$uri;
        if (array_key_exists('port', $p))
            $uri .= ':'.$p['port'];
        if (array_key_exists('path', $p))
            $uri .= $p['path'];
        $uri = $uri.'/api/'.$resource.'?schema=blank';
        $content = $this->fc->get($uri, false);
        return simplexml_load_file($content);
    }
    
    function send(array $prod)
    {
        $this->initSchemXml();
        $this->id = 0;
        $this->id_supplier = 0;
        $this->hasimage = 0;
        $this->active = self::$visible;
        $this->sendProd($prod);

        unset($this->cur_price);
        unset($this->cur_active);
        unset($this->cur_id_supplier);
        unset($this->cur_ref_supplier);

        echo PHP_EOL;
    }

    /**
     * @param string $resource
     * @param string $data_xml
     * @param SimpleXMLElement $data_xml
     * @return array
     */
    function getOpt($resource, $data_xml, $data_type = 'postXml')
    {
        return [
            'resource' => $resource,
            $data_type => $data_xml->asXML()
        ];
    }
    function getOptPost($resource, $data_xml)
    {
        return $this->getOpt($resource, $data_xml, 'postXml');
    }
    function getOptPut($resource, $data_xml, $id)
    {
        $data = $this->getOpt($resource, $data_xml, 'putXml');
        $data['id'] = $id;
        return $data;
    }

    function getOptGetByName($resource, $filtername, $display = '[id]')
    {
        return [
            'resource' => $resource,
            'display' => $display,
            'filter[name]'=> '['.$filtername.']'
        ];
    }

    function maxLineCheckHtml($value, $len)
    {
        $i = 0;
        while ($i < 4) {
            if ($value[$len + $i] === '>') {
                $y = $len;
                while ($y) {
                    $y -= 1;
                    if ($value[$y] === '<') {
                        return  $y;
                    }
                }
                return $len;
            }
            $i += 1;
        }
        return $len;
    }
    
    function maxLine(&$value, $max_len, $end='...')
    {
        $len = strlen($value);
        if ($len >= 0 && $len > $max_len) {
            $len = $len - ($len - ($max_len - 4));

            $len = $this->maxLineCheckHtml($value, $len);
            //$value = substr($value, 0, $len);
            // multi-octet
            if ($len > 7 && !empty($end)) {
                $value = mb_strcut($value, 0, $len - 3) . '...';
            }
            else
                $value = mb_strcut($value, 0, $len);
        }
    }

    public static $link_rewrite_search = [
        ' ','!','"','#','$','%','&',"'",'(',')','*','+',
        ',','-','.','/',':',';','<','=','>','?','[','@',
        '\\',']','^','_','`','{','|','}','~'
    ];
    public static $link_rewrite_replace = [
        '-','-','-','-','-','-','-','-','-','-','-','-',
        '-','-','-','-','-','-','-','-','-','-','-','-',
         '-','-','-','-','-','-','-','-','-'
    ];
    function genLink_rewrite($name) {
        $link_rewrite = str_replace(
            self::$link_rewrite_search,
            self::$link_rewrite_replace,
            $name
        );
        $link_rewrite = iconv('UTF-8', 'ASCII//IGNORE', $link_rewrite);
        $this->maxLine($link_rewrite, 128, null);
        return $link_rewrite;
    }

    public static $replace_in_name = [
        '>',          '=',    '&',    '#',  ';','<'
    ];
    public static $replace_ou_name = [
        'supérieur à','égale','et','&#135;',',','inférieur à'
    ];
    function genName(&$name, $maxline = 128) {
        $name_link = str_replace(
            self::$replace_in_name,
            self::$replace_ou_name,
            $name
        );
        $this->maxLine($name_link, $maxline, null);
        return $name_link;
    }
    
    function genLinkRewriteAndName($name_link, &$xml, $id_lang)
    {
        $link_rewrite = $this->genLink_rewrite($name_link);
        $name_link = $this->genName($name_link);
        $c = $xml->name->addChild('language', $name_link); // <language>$name_link</language>
        $c->addAttribute('id', $id_lang); /* <language id=$id_lang>$name_link</language> */
        $c = $xml->link_rewrite->addChild('language', $link_rewrite);
        $c->addAttribute('id', $id_lang);
    }
}
?>

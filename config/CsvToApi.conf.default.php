<?php

require 'CsvToApi_constant.php';

define('CSVTOAPI_DEBUG', false);
// Importer seulement les produits qui sont référencé chez icecat
define('IMPORT_ONLY_ICECAT', 1);

define('ICECAT_USER', 'user');
define('ICECAT_PASS', 'password');
define('ICECAT_URI', 'data.icecat.biz');
define('ICECAT_LANG', 'FR');
// duree du cache en seconde
define('ICECAT_TIME_CACHE', 259200);

// >= php 7
define('FILE_CSV', [
    'fournisseur1.csv',
    'fournisseur2.csv'
]);
// < php 7
//define('FILE_CSV', 'onlyfile.csv');

define('API_TOKEN', 'XXXXXXXXXXXXXXXXXXXXXXXX');// contenue du fichier .key
define('API_KEY', false); // prestashop debug
define('API_URI', 'https://localhost/path/');
define('API_CLASS', 'ToPrestashop\ToPrestashop');
/* define('API_CLASS', 'ToTheliaApi\ToTheliaApi'); */

//// thelia
/* define('DB_API_HOST', 'localhost'); */
/* define('DB_API_USER', 'user'); */
/* define('DB_API_PASS', 'pass'); */
/* define('DB_API_TABLE', 'thelia_test'); */
/* define('DB_API_CHARSET', 'utf8'); */
// gabarit par defaut pour les caracteristique
/* define('API_GAB_DEFAULT', 1); */
// id fournisseur dans gabarit->attribut
/* define('API_GAB_ATT_SUPPLIER', 1); */
////

// Commun
// Id de la categorie parent par defaut, exemple catégorie 'non trèe'
define('API_DEFAULT_CATEGRORY', 35);
// Utiliser pour les catégories, quel fournisseur prend le pas ?
// Si la base de relation(fournisseur<->icecat) est déjà fait alors mette null
define('FOURNISSEUR_PRINCIPAL', 'ALSO');
// Active la gestion des caractèristiques
define('ENABLE_FEATURE', true);
// Active la gestion des images
define('ENABLE_IMAGE', true);
// Active la création des catégories front
// depuis le fournisseur principale
// a activé si les relation n'existe pas
define('CREATE_CATEGORY', true);

define('DB_COEF_HOST', 'localhost');
define('DB_COEF_USER', 'xxxxx');
define('DB_COEF_PASS', 'xxxx');
define('DB_COEF_TABLE', 'coef');

define('PATHTMP', dirname(__FILE__) . '/tmp');

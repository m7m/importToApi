<?php

$i = 0;
define('INDEX_FOURNISSEUR', $i++);
define('INDEX_EAN_CODE', $i++);
define('INDEX_REF_CONSTRUCTEUR', $i++);
define('INDEX_REF_FOURNISSEUR', $i++);
define('INDEX_LIBELLE_PRODUIT', $i++);
define('INDEX_MARQUE', $i++);
define('INDEX_CATEGORIES', $i++);

define('INDEX_PRIX_HTT', $i++);
define('INDEX_ECOTAX', $i++);
define('INDEX_SACEM', $i++);
define('INDEX_COPIE_PRIVEE', $i++);
define('INDEX_DISPONIBILITE', $i++);
define('INDEX_POID', $i++);
define('INDEX_IMAGES', $i++);

define('INDEX_DESCRIPTIONS_COURT', $i++);
define('INDEX_DESCRIPTIONS_LONG', $i++);
define('INDEX_CARACTERISTIQUE', $i++);

define('INDEX_VISIBLE', $i++);
define('INDEX_NOUVEAU', $i++);

define('INDEX_DESCRIPTIONS_COURT_LANG', $i++);
define('INDEX_DESCRIPTIONS_LONG_LANG', $i++);
define('INDEX_LIBELLE_PRODUIT_LANG', $i++);
define('INDEX_CATEGORIES_LANG', $i++);
define('INDEX_IMAGES_LANG', $i++);
define('INDEX_CARACTERISTIQUE_LANG', $i++);

define('INDEX_CATEGORIES_ICECAT', $i++);
define('INDEX_CATEGORIES_ICECAT_LANG', $i++);

unset ($i);

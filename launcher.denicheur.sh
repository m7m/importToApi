#!/bin/sh

WORK_PATH="$(dirname $0)"
cd $WORK_PATH

if [ ! -e 'config/launcher.denicheur.conf' ];then
	echo 'Fichier de configuration inexistant: config/launcher.denicheur.conf'
	exit 1
fi
. config/launcher.denicheur.conf

exec 1>$FILE_LOG
exec 2>&1

send_mel()
{
	return 
    SUBJECT="$1"
    if [ -s $FILE_LOG ];then
	OPT="-a $FILE_LOG"
    else
	OPT=""
    fi
    (
	if [ -s $FILE_LOG ];then
	    echo 'Log ci-joint'
	fi
    ) | /usr/bin/mailx -s "$SUBJECT" $OPT $ADDRE
}

#mysql a besoin de SET GLOBAL log_bin_trust_function_creators = 1;

func_opt_sql_add_func()
{
    mysql -u $USER -p$MDP -D $BDD_SQL \
	  -e "
DROP FUNCTION IF EXISTS foncConcatCategoryName;
DELIMITER //
CREATE DEFINER = CURRENT_USER() FUNCTION foncConcatCategoryName ( id_cat INT, id_lang INT)
RETURNS TEXT(2048) DETERMINISTIC
BEGIN
   SET @id = id_cat;
   SET @cat_name_concat = NULL;
   label1: WHILE @id > 2 DO
	SELECT c.id_category,
	 cl.name,
	 c.id_parent
	 INTO @id_cat, @cat_name, @id_parent
 	 FROM ps_category AS c,
	 ps_category_lang AS cl
 	 WHERE c.id_category = @id
	 AND cl.id_category = c.id_category
	 AND cl.id_lang = id_lang;
	 SET @id = @id_parent;
	 IF @cat_name_concat IS NULL THEN
	 	 SET @cat_name_concat = @cat_name;
	 ELSE
	 	 SET @cat_name_concat = CONCAT(@cat_name, '>', @cat_name_concat);
	 END IF;
   END WHILE label1;
   RETURN @cat_name_concat;
END; //
DELIMITER ;
"
    if [ $? -ne 0 ];then
	echo 'Error add function'
	send_mel 'Extract denicheur: Error add function'
	exit 1
    fi
} # func_opt_sql_add_func

fonc_opt_denicheur_tout_pro_sql()
{
    reste=$1
    sortie=$2
    limit=$3

    rm $EMP_SQL_DENI_GLOBAL 2>/dev/null
    mysql $OPT_SQL -D $BDD_SQL \
	-e "
use $BDD_SQL;

set @TVA = $TVA;
set @id_lang = $ID_LANG;
set @reste = $reste; -- n'as pas atteint les 2000 on repase en ignorant les categories précédentes
set @url=(select CONCAT(domain_ssl,physical_uri) from ps_shop_url where id_shop_url = 1);
set @proto = 'https://';

select 
pl.name as name,
ps.id_product as id_product,
pm.name as marque,
foncConcatCategoryName(cl.id_category, @id_lang) as categories,
'Neuf' as Etat,
truncate(p.price * @TVA,2) as PVTTC,

CASE
WHEN p.weight >=  0 && p.weight < 0.250001 THEN truncate(4.900000*@TVA,2)
 WHEN p.weight >= 0.250001 && p.weight < 0.500001 THEN truncate(6.100000*@TVA,2)
  WHEN p.weight >= 0.500001 && p.weight < 0.750001 THEN truncate(6.900000*@TVA,2)
   WHEN p.weight >= 0.750001 && p.weight < 1.000001 THEN truncate(7.500000*@TVA,2)
    WHEN p.weight >= 1.000001 && p.weight < 2.000001 THEN truncate(8.500000*@TVA,2)
     WHEN p.weight >= 2.000001 && p.weight < 5.000001 THEN truncate(12.900000*@TVA,2)
      WHEN p.weight >= 5.000001 && p.weight < 10.000001 THEN truncate(18.900000*@TVA,2)
       WHEN p.weight >= 10.000001 && p.weight < 30.000001 THEN truncate(26.900000*@TVA,2)
           ELSE truncate(26.900000*@TVA,2)
END AS frais_port,

CASE
  WHEN sa.quantity > 3
      THEN 'En stock - qté > 3'
  WHEN sa.quantity <= 3
      THEN 'Stock limité qté <= 3'
END as disponibilite,

concat(@proto,@url,'lang/',p.id_product,'-',pl.link_rewrite,'.html') as url_produit_trk,
concat(@proto,@url,'lang/',p.id_product,'-',pl.link_rewrite,'.html') as url_produit_ntrk,
3 as delai,
ps.product_supplier_reference as ref_fournisseur,
p.reference as ref_fabricant,
p.ean13,
concat(@proto,@url, pi.id_image, '-large_default/', pl.link_rewrite, '.jpg') as image

INTO OUTFILE '$EMP_SQL_DENI_GLOBAL'
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '\"'
LINES TERMINATED BY '\n'

from
ps_product as p,
ps_product_lang as pl,
ps_manufacturer as pm,
ps_product_supplier as ps,
ps_category_product AS cp,
ps_category_lang AS cl,
ps_image as pi,
ps_stock_available as sa

where 
    pm.id_manufacturer = p.id_manufacturer
and ps.id_product = p.id_product
and ps.id_product is not null
and ps.id_product != ''
and p.id_product = cp.id_product
and cp.id_category = cl.id_category
and p.id_product = pl.id_product
and p.id_product = pi.id_product
and p.id_product = sa.id_product
and cl.id_lang = @id_lang
and p.active = 1
and sa.quantity > 0

group by p.id_product
HAVING categories is not null
"
    if [ $? -ne 0 ];then
	echo 'Error extract global bdd'
	send_mel 'Extract denicheur: Error extract global bdd'
	exit 1
    fi
    mv "$EMP_SQL_DENI_GLOBAL" "$sortie"
    if [ $? -ne 0 ];then
	echo 'Error mv file'
	send_mel 'Extract denicheur: Error mv file'
	exit 1
    fi
} # end fonc_opt_denicheur_tout_pro_sql

fonc_opt_denicheur_exec()
{
    func_opt_sql_add_func
    
    rm $EMP_FINAL_DENI_GLOBAL 2>/dev/null
    fonc_opt_denicheur_tout_pro_sql 0 "$EMP_FINAL_DENI_GLOBAL" ""
    send_mel 'Extract denicheur: ok'
}

fonc_opt_denicheur_exec

#!/bin/bash

# Permet de repositionner les catégories en ordre alphabétique

cd "$(dirname $0)"

if [ ! -e 'config/prestashop_postion_alpha.conf' ];then
	echo 'Fichier de configuration inexistant: config/prestashop_postion_alpha.conf'
	exit 1
fi
. config/prestashop_postion_alpha.conf

list_id_parent="$(mysql $OPTS -e 'select DISTINCT id_parent from ps_category where id_parent >= 2')"

for id_parent in $list_id_parent
do
	list_id_cat="$(mysql $OPTS \
		  -e 'SELECT c.id_category

FROM ps_category AS c,
 ps_category_lang AS cl

WHERE c.id_parent = '$id_parent'
 AND c.id_category = cl.id_category
 AND cl.id_lang = 1
ORDER BY cl.name;'
			   )"

	i=1
	q=''
	for id_cat in $list_id_cat
	do
		q+='UPDATE ps_category SET position = '$i' WHERE id_category = '$id_cat';'
		q+='UPDATE ps_category_shop SET position = '$i' WHERE id_category = '$id_cat';'
		i=$(($i+1))
	done
	mysql $OPTS -e "$q"
done

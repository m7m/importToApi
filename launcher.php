<?php

/**
 * Lanceur de paramétrage et d'éxecution de csvToApi
 *
 * PHP version 7
 *
 * @category Importation
 * @package  CsvToApi
 * @author   M.Michel <m dot michel at m7m dot fr>
 * @license  Gnu public licence version 3
 * @link     None
 */

require 'inc/load.php';

/**
 * Traite le fichier d'entrée
 *
 * @param string $file file csv
 *
 * @return None
 */
function launcher($filecsv)
{
    $o = new CsvToApi\csvToApiOverIcecat(
        ICECAT_USER,
        ICECAT_PASS,
        ICECAT_URI,
        ICECAT_LANG,
        $filecsv,
        API_TOKEN,
        API_KEY,
        API_URI,
        API_CLASS
    );
    $o::$tmp_path = PATHTMP;
    $o::$only_icecat = IMPORT_ONLY_ICECAT;

    // thelia api incomplete
    /* $o->fapi->db = new mysqli( */
    /*     DB_API_HOST, */
    /*     DB_API_USER, */
    /*     DB_API_PASS, */
    /*     DB_API_TABLE */
    /* ); */
    /* $o->fapi->db->set_charset(DB_API_CHARSET); */
    /* // gabarie par defaut */
    /* $o->fapi->id_template = API_GAB_DEFAULT; */
    /* // id fournisseur dans gabarit->attribut */
    /* $o->fapi->supplier_id_attribute = API_GAB_ATT_SUPPLIER; */
    /* // categorie parent par defaut */
    /* $o->fapi->default_category_id = API_DEFAULT_CATEGRORY; */
    //

    $o->fapi::$time_cache_sec = ICECAT_TIME_CACHE;
    $o->db_coef = new mysqli(
        DB_COEF_HOST,
        DB_COEF_USER,
        DB_COEF_PASS,
        DB_COEF_TABLE
    );

	// relation categories fournisseur avec icecat
    $o->fapi->rfi = new RelFourIcecat\relFourIcecat(
        DB_COEF_HOST,
        DB_COEF_USER,
        DB_COEF_PASS,
        DB_COEF_TABLE
    );
    
    $o->icecat->tmp = PATHTMP;
    $o->fapi::$tmp = PATHTMP;

    $o->fapi::$visible = 1;
    $o->fapi::$default_parent_category_id = API_DEFAULT_CATEGRORY;
    $o->fapi::$default_disable_category_id = API_DEFAULT_CATEGRORY;
    $o->fapi::$defaut_fournisseur_name = FOURNISSEUR_PRINCIPAL;
    $o->fapi::$enable_feature = ENABLE_FEATURE;
    $o->fapi::$enable_image = ENABLE_IMAGE;
    $o->fapi::$create_category = CREATE_CATEGORY;
    $o->fapi->fc->tmp = PATHTMP;
    
    /* $o->send(); */ // import complet
	$o->update(); // import avec comparaisont sur l'ancien fichier d'import
}

/**
 * Permet éxecution de plusieur csv l'un après l'autre
 *
 * @return None
 */
function Launcher_mutlifile()
{
    if (is_array(FILE_CSV)) {
        $multifile = FILE_CSV;
    } else {
        $multifile = [FILE_CSV];
    }
    foreach ($multifile as $f) {
        if (file_exists($f)) {
            launcher($f);
        } else {
            echo 'Not file:'.$f.PHP_EOL;
        }
    }
}

Launcher_mutlifile();
//launcher(FILE_CSV);

?>